var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    rimraf = require('rimraf'),
    autoprefixer = require('gulp-autoprefixer'),
    cssNano = require('gulp-cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    sass = require('gulp-sass'),
    panini = require('panini'),
    include = require('gulp-include'),
    babel = require('gulp-babel'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    runSequence = require('run-sequence');

// Проверка флага запуска --prod
var PRODUCTION = true;
// Проверка флага запуска --m
var MINIFICATION = !!(argv.m);

var
    buildRoot = 'dist/',
    srcRoot = 'src/';

var paniniSrc = {
    root: srcRoot + 'html/pages/',
    layouts: srcRoot + 'html/layouts/',
    partials: srcRoot + 'html/partials/',
    helpers: srcRoot + 'html/helpers/',
    data: srcRoot + 'html/data/'
};

var build = {
    main: buildRoot,
    js: buildRoot + 'js/',
    styles: buildRoot + 'css/',
    img: buildRoot + 'pic/',
    favicons: buildRoot + 'favicons/',
    hidden: buildRoot + 'hidden/',
    fonts: buildRoot + 'fonts/'
};

var src = {
    pages: [paniniSrc.root + '**/*.html'],
    js: [srcRoot + 'js/**/*.js', '!' + srcRoot + 'js/**/*.min*', '!' + srcRoot + 'js/_*/*'],
    minJs: [srcRoot + 'js/**/*.min.{js,json}', '!' + srcRoot + 'js/_*/*'],
    json: [srcRoot + 'js/**/*.json', '!' + srcRoot + 'js/_*/*'],
    styles: [srcRoot + 'styles/**/*.scss'],
    img: [srcRoot + 'pic/**/*.{jpeg,jpg,png,gif,webp,ico,cur}'],
    favicons: [srcRoot + 'favicons/*', '!' + srcRoot + 'favicons/favicon.ico', '!' + srcRoot + 'favicons/browserconfig.xml'],
    favicon: [srcRoot + 'favicons/**/favicon.ico'],
    browserConfig: [srcRoot + 'favicons/**/browserconfig.xml'],
    hidden: [srcRoot + 'hidden/**/*'],
    fonts: [srcRoot + 'fonts/**/*']
};

gulp.task('pages', function () {
    gulp.src(src.pages)
        .pipe(plumber())
        .pipe(panini(paniniSrc))
        .pipe(gulp.dest(build.main));
});

gulp.task('pages:refresh', function () {
    panini.refresh();
});

gulp.task('pages:reset', function (callback) {
    runSequence('pages:refresh','pages', callback);
});

gulp.task('styles', function () {
    gulp.src(src.styles)
        .pipe(plumber())
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
        }))
        .pipe(gulpif(MINIFICATION,
            rename({
                suffix: '.min'
            })))
        .pipe(gulpif(MINIFICATION,
            cssNano({
                discardUnused: false,
                autoprefixer: false,
                zindex: false
            })))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write('sourcemaps')))
        .pipe(gulp.dest(build.styles));
});

gulp.task('scripts-js', function () {
    gulp.src(src.js)
        .pipe(plumber())
        .pipe(include())
        .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
        .pipe(babel(
            {
                "presets": ["es2015"],
                "compact": true,
                "only": ['./src/js/libs/foundation.js']
            }
        ))
        .pipe(gulpif(MINIFICATION,
            rename({
                suffix: '.min'
            })))
        .pipe(gulpif(MINIFICATION,
            uglify()))
        .pipe(gulpif(!PRODUCTION, sourcemaps.write('sourcemaps')))
        .pipe(gulp.dest(build.js));
});

gulp.task('scripts-min-js', function () {
    gulp.src(src.minJs)
        .pipe(plumber())
        .pipe(gulp.dest(build.js));
});

gulp.task('scripts-json', function () {
    gulp.src(src.json)
        .pipe(plumber())
        .pipe(gulp.dest(build.js));
});

gulp.task('scripts', ['scripts-js', 'scripts-min-js', 'scripts-json']);

gulp.task('images', function () {
    return gulp.src(src.img)
        .pipe(plumber())
        .pipe(cache(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(build.img));
});

gulp.task('fonts', function () {
    gulp.src(src.fonts)
        .pipe(plumber())
        .pipe(gulp.dest(build.fonts));
});

gulp.task('hidden', function () {
    gulp.src(src.hidden)
        .pipe(plumber())
        .pipe(gulp.dest(build.hidden));
});

gulp.task('fav-ico', function () {
    gulp.src(src.favicon)
        .pipe(plumber())
        .pipe(gulp.dest(build.main));
});

gulp.task('fav-config', function () {
    gulp.src(src.browserConfig)
        .pipe(plumber())
        .pipe(gulp.dest(build.main));
});

gulp.task('favicons', ['fav-ico', 'fav-config'], function () {
    gulp.src(src.favicons)
        .pipe(plumber())
        .pipe(gulp.dest(build.favicons));
});

gulp.task('clean', function (cb) {
    rimraf(build.main, cb);
});

gulp.task('build', function(callback) {
    runSequence('clean', ['pages', 'styles', 'scripts', 'images', 'fonts', 'hidden', 'favicons'], callback);
});

gulp.task('watch', function () {
    gulp.watch([src.pages], ['pages']);
    gulp.watch([srcRoot + 'html/{layouts,partials,helpers,data}/**/*'], ['pages:reset']);
    gulp.watch([src.styles], ['styles']);
    gulp.watch([src.js], ['scripts-js']);
    gulp.watch([src.minJs], ['scripts-min-js']);
    gulp.watch([src.json], ['scripts-json']);
    gulp.watch([src.img], ['images']);
    gulp.watch([src.fonts], ['fonts']);
    gulp.watch([src.hidden], ['hidden']);
    gulp.watch([src.favicons, src.favicon, src.browserConfig], ['favicons']);
});

gulp.task('default', ['build', 'watch']);
