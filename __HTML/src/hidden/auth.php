<div class="mfiModal big zoomAnim auth-popup">
    <script>
        jQuery(document).ready(function($) {
            $('#forget_pass').on('click', function(event) {
                $('#entrForm').removeClass('visForm');
                $('#forgetForm').addClass('visForm');
            });

            $('#remember_pass').on('click', function(event) {
                $('#forgetForm').removeClass('visForm');
                $('#entrForm').addClass('visForm');
            });

            $('#enterReg').on('click', '.erTitle', function(event) {
                event.preventDefault();
                if($(window).width() < 720) {
                    if(!$(this).parent().hasClass('wCur')) {
                        $('#enterReg .popupBlock').removeClass('wCur').filter($(this).parent()).addClass('wCur');
                    }
                }
            });
        });
    </script>
    <div class="popup-header">
        <div class="popup-header__img-holder">
            <img src="pic/logo.png" alt="homedics">
        </div>
    </div>
    <div id="enterReg" class="popup-body">
        <div class="enterReg_top">
            <div class="popupBlock enterBlock wCur">
                <div class="erTitle">Вход на сайт</div>
                <div class="popupContent">
                    <div id="entrForm" data-form="true" class="wForm wFormDef enterBlock_form visForm">
                        <div class="wFormRow">
                            <input class="wInput" type="email" name="enter_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                            <div class="inpInfo">E-mail</div>
                        </div>
                        <!-- .wFormRow -->
                        <div class="wFormRow">
                            <input class="wInput" type="password" name="enter_pass" data-rule-password="password" data-msg-password="Укажите корректный пароль!" data-msg-required="Это поле необходимо заполнить" placeholder="Пароль" required="">
                            <div class="inpInfo">Пароль</div>
                        </div>
                        <!-- .wFormRow -->
                        <label class="wCheck" style="margin-bottom: 2rem;">
                            <input type="checkbox" checked="checked">
                            <ins></ins>
                            <span>Запомнить данные</span>
                        </label>
                        <!-- .checkBlock -->
                        <div class="text-right">
                            <button class="wSubmit button">ВОЙТИ</button>
                        </div>
                        <div class="tac">
                            <span class="passLink" id="forget_pass">Забыли пароль?</span>
                        </div>
                    </div>
                    <div id="forgetForm" data-form="true" class="wForm wFormDef enterBlock_form">
                        <div class="wFormRow">
                            <input class="wInput" type="email" name="forget_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                            <div class="inpInfo">E-mail</div>
                        </div>
                        <!-- .wFormRow -->
                        <div class="forgetInf">
                            После отправления, в течении 5 минут к Вам на почту придут инструкции по восстановлению пароля.
                        </div>
                        <!-- .forgetInf -->
                        <div class="passLink" id="remember_pass">Вернуться</div>
                        <div class="text-right">
                            <button class="wSubmit button">ОТПРАВИТЬ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .enterBlock -->
            <div data-form="true" class="popupBlock wForm wFormDef regBlock">
                <div class="erTitle">Новый покупатель</div>
                <div class="popupContent">
                    <div class="wFormRow">
                        <input class="wInput" type="email" name="reg_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                        <div class="inpInfo">E-mail</div>
                    </div>
                    <!-- .wFormRow -->
                    <div class="wFormRow">
                        <input class="wInput" type="password" name="reg_pass" data-rule-password="password" data-msg-password="Укажите корректный пароль!" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
                        <div class="inpInfo">Пароль</div>
                    </div>
                    <!-- .wFormRow -->
                    <label class="wCheck">
                        <input type="checkbox" name="reg_agree" data-msg-required="Это поле нужно отметить" required="">
                        <ins></ins>
                        <span>Принимаю условия использования и обработки моих персональных данных</span>
                    </label>
                    <!-- .checkBlock -->
                    <div class="text-left">
                        <button class="wSubmit button">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                    </div>
                </div>
                <!-- .popupContent -->
            </div>
            <!-- .regBlock -->
        </div>
        <!-- .enterReg_top -->
        <div class="popupBlock socEnter">
            <div class="erTitle">Вход через соц. сети</div>
            <div class="popupContent socLinkEnter">
                <div class="socials">
                    <button type="button" title="facebook" class="socials__link">
                        <i class="socials__icon">
                            <svg>
                                <use xlink:href="#icon_facebook"></use>
                            </svg>
                        </i>
                    </button>
                    <button type="button" title="twitter" class="socials__link">
                        <i class="socials__icon">
                            <svg>
                                <use xlink:href="#icon_twitter"></use>
                            </svg>
                        </i>
                    </button>
                    <button type="button" title="pinterest" class="socials__link">
                        <i class="socials__icon">
                            <svg>
                                <use xlink:href="#icon_pinterest"></use>
                            </svg>
                        </i>
                    </button>
                    <button type="button" title="instagram" class="socials__link socials__link--instagram">
                        <i class="socials__icon">
                            <svg>
                                <use xlink:href="#icon_instagram"></use>
                            </svg>
                        </i>
                    </button>
                    <button type="button" title="google+" class="socials__link socials__link--gp">
                        <i class="socials__icon">
                            <svg>
                                <use xlink:href="#icon_google_plus"></use>
                            </svg>
                        </i>
                    </button>
            </div>
            <!-- .socLinkEnter -->
            <div class="clear"></div>
        </div>
        <!-- .socEnter -->
    </div>
    <!-- #enterReg -->
</div>
</div>