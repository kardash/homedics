<div class="mfiModal medium zoomAnim oneclick-popup">
    <script>
        jQuery(document).ready(function ($) {
            $('.inputmask').inputmask();
        });
    </script>
    <div class="popup-header">
        <div class="popup-header__img-holder">
            <img src="pic/logo.png" alt="homedics">
        </div>
    </div>
    <div class="popup-body is-list-view">
        <div class="product-preview">
            <a class="product-preview__image-container" href="item.html" title="Quad Shiatsu Massage Cushion with Heat">
                <img src="pic/img-product-1.jpg" alt="Quad Shiatsu Massage Cushion with Heat">
            </a>
            <div class="product-preview__top-pane text-center medium-text-left">
                <a class="product-preview__name product-preview__name--large product-preview__for-list-view color--white"
                   href="item.html" title="Quad Shiatsu Massage Cushion with Heat"><span>Quad Shiatsu Massage Cushion with Heat</span></a>
                <div class="product__rating-container">
                    <span class="product__code font-size-small font-family-cgothic">Код товара: MCS-750H</span>
                </div>
            </div>
        </div>
        <div class="wForm wFormDef" data-form="true">
            <div class="row">
                <div class="column large-6">
                    <div class="wFormInput">
                        <input type="text" class="wInput" required
                               name="oneclick_name"
                               placeholder="Ваше имя"
                               data-rule-word="true"
                               data-rule-minlength="2"
                               data-msg-required="Пожалуйста, укажите ваше имя!"
                               data-msg-word="Имя не должно содержать цифры, знаки препинания и спец. символы!"
                               data-msg-minlength="Введите не меньше 2-х букв!">
                        <div class="inpInfo">Имя*</div>
                    </div>
                </div>
                <div class="column large-6">
                    <div class="wFormInput">
                        <input type="tel" class="wInput inputmask" required
                               placeholder="Номер телефона"
                               name="oneclick_phone"
                               data-inputmask="'mask': '+380999999999'"
                               data-rule-phoneUA="true"
                               data-msg-phoneUA="Укажите корректный номер +38ХХХХХХХХХХ"
                               data-msg-required="Пожалуйста, укажите номер телефона!">
                        <div class="inpInfo">Номер телефона*</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column text-center">
                    <button class="wSubmit button alert" type="submit">ПОДТВЕРДИТЬ ЗАКАЗ</button>
                </div>
            </div>
        </div>
    </div>
</div>