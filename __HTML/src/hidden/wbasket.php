<?php
	header('Content-Type: application/json');
		echo '{
			"list": [
				{
					"id": "basketItem123",
					"artikul": "UHE-CM24",
					"link": "item.html",
					"src": "pic/img-lot-1.jpg",
					"title": "Портативный увлажнитель Mist Humidifier",
					"count": 2,
					"price": 740,
					"price_old": 899,
					"priceall": 1480,
					"mincount": 1,
					"maxcount": 17
				}, {
					"id": "basketItem356",
					"artikul": "UHE-CM15",
					"link": "item.html",
					"src": "pic/img-lot-2.jpg",
					"title": "Atlas Vibration Acu-Node Massager",
					"description": "<p>HoMedics® Портативный массаж спины подушки обеспечивает бодрящий массаж вибрации для всей спине. В комплекте с комплексной системой управления, позволяющей выбирать между высоким или низким настроек массажа, задняя массажные подушки идеально подходит для любого случая.</p>",
					"count": 1,
					"price": 999,
					"price_old": 1000,
					"priceall": 999,
					"mincount": 1,
					"maxcount": 17
				}
			],
			"totals": {
				"total_quantity": 3,
				"total_price": 9999
			},
			"empty": 0
		}';
?>