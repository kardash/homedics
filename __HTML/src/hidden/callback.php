<div class="mfiModal zoomAnim auth-popup">
    <div class="popup-header">
        <div class="popup-header__img-holder text-center">
            <img src="pic/logo.png" alt="homedics">
        </div>
    </div>
    <div class="popup-body">
        <div class="section-title text-center"><span>ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</span></div>
        <div class="font-size-small font-family-gothic text-center" style="line-height: 1.2em; margin-bottom: 2rem;"><span>ОСТАВЬТЕ СВОЙ НОМЕР ТЕЛЕФОНА И МЫ ВАМ ПЕРЕЗВОНИМ</span></div>
        <div class="wForm wFormDef" data-form="true">
            <div class="wFormRow">
                <div class="wFormInput">
                    <input type="tel" class="wInput inputmask" required
                           name="contacts_phone"
                           placeholder="Номер телефона"
                           data-inputmask="'mask': '+380999999999'"
                           data-rule-phoneUA="true"
                           data-msg-phoneUA="Укажите корректный номер +38ХХХХХХХХХХ"
                           data-msg-required="Пожалуйста, укажите номер телефона!">
                    <div class="inpInfo">*Номер телефона</div>
                </div>
            </div>
            <div class="wFormRow text-center">
                <button class="wSubmit button alert">ОТПРАВИТЬ</button>
            </div>
        </div>
    </div>
</div>