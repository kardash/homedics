﻿wPreloader.config({
    markup: ''+
        '<div id="fountainG">'+
            '<div id="fountainG_1" class="fountainG"></div>'+
            '<div id="fountainG_2" class="fountainG"></div>'+
            '<div id="fountainG_3" class="fountainG"></div>'+
            '<div id="fountainG_4" class="fountainG"></div>'+
            '<div id="fountainG_5" class="fountainG"></div>'+
            '<div id="fountainG_6" class="fountainG"></div>'+
            '<div id="fountainG_7" class="fountainG"></div>'+
            '<div id="fountainG_8" class="fountainG"></div>'+
        '</div>'
});

function togglePreloader(instance, toggle) {
    var preloader = instance.structure.preloader.element;
    wPreloader[toggle](preloader);
}

wBasket.debugg = false;

function cloneCard() {
    var block= $('.js-basket-clone');
    var instList = wBasket.instance.response.list;
    var instArray = [];
    for (var i = 0; i < instList.length; i++) {
        var item = instList[i];
        var string = ''+
            '<div class="product-list__item">' +
                '<div class="product-list__img-container">' +
                    '<a href="' +item.link+ '" title="' +item.title+ '">' +
                        '<img src="'+item.src+'" alt="' +item.title+ '">' +
                    '</a>' +
                '</div>' +
                '<div class="product-list__item-info product-list__item-info--price-middle">' +
                    '<div class="product-list__title">' +
                        '<a href="' +item.link+ '" title="' +item.title+ '">' +item.title+ '</a>' +
                    '</div>' +
                    '<div class="product-list__quantity">' +
                        '<span>'+item.count+'</span> шт.' +
                    '</div>' +
                    '<div class="product-list__price price">' +
                        '<span class="price__old"><ins>' +item.price_old+ '</ins> грн.</span>&nbsp;' +
                        '<span class="price__current"><ins>' +item.price+ '</ins> грн.</span>' +
                    '</div>' +
                '</div>' +
            '</div>';
        instArray.push(string);
    }
    block.html(instArray.join('\n'));
}

jQuery(document).ready(function($) {

    $('.onChangeToMfiB').on('change', 'input', function(event) {
        var ths = $(this);
        var value = ths.val();
        var group = ths.data('group');
        var parent = ths.closest('.setToMfiB');
        var mfib = parent.find('.mfiB');
        var param = mfib.data('param');
        param[group] = value;
    });

    wBasket.Config({
        writePage: false,
        delayReDraw: 700,
        unicName: 'id',
        sendTo: {
            total_quantity: ['.js-basket-quantity'],
            total_price: ['.js-basket-total']
        }
    });

    function _setButton(elem, def) {
        var _action = 'new';
        var _func = 'removeClass';
        if (def) {
            _action = 'default';
            _func = 'addClass';
        }
        elem.data('param').action = _action;
        elem[_func]('className');
    }

    function _findButton(ID) {
        $('.mfiB').each(function(index, el) {
            var _el = $(el);
            var _id = _el.data('param').id;
            if (_id == ID) {
                _setButton(_el, false);
            }
        });
    }

    wBasket.Callbacks({
        afterDraw: function(instance) {
            cloneCard();
        },
        afterWrite: function(instance) {
            cloneCard();
        },
        beforeDelete: function(instance, iD, item, itemData, confirm) {
            var wb = this;
            togglePreloader(instance, 'hide');
            item.find('.inputDelConfirm').trigger('focus');
            confirm.on('click', function(event) {
                event.preventDefault();
                wb.proto.runCallback('onDelete', [iD, item, itemData]);
            });
        },
        onDelete: function(instance, iD, item, itemData) {
            var wb = this;
            togglePreloader(instance, 'show');
            /* ajax */
            /* ответе */
            item.stop().slideUp(500, function() {
                item.remove();
                wb.ReDraw('footer', {
                    totals: {
                        "total_qauntity": 2,
                        "total_price": 1800
                    }
                }, iD);
                _findButton();
            });
            cloneCard();
        },
        afterSend: function(instance) {
            cloneCard();
        },
        onEdit: function(instance, iD, item, itemData, editData) {
            var wb = this;
            togglePreloader(instance, 'show');

            /* demo */
                $.ajax({
                    url: 'hidden/wbasket.php',
                    dataType: 'json',
                    statusCode: {
                        404: function() {
                            alert("Список товаров не найден!!!");
                        }
                    }
                }).fail(function() {
                    alert("Ошибка!\nЗапрос провалился!!!");
                }).done(function(data) {
                    wb.ReDraw('all', data);
                });
            cloneCard();
        },
        afterReDraw: function(instance, element) {
            togglePreloader(instance, 'hide');
            cloneCard();
        },
        onEmpty: function (instance) {
            instance.structure.footer.element.html('<div class="font-size-large">В корзине нет товаров</div>');
        }
    });

    wBasket.Structure({
        basket: {
            classCss: 'wBasketModule wb_animate',
            markup: ''+
                '<div class="wBasketWrapp">'+
                    '<div class="wBasketHead"></div>'+
                    '<div class="wBasketBody"></div>'+
                    '<div class="wBasketFooter"></div>'+
                '</div>'+
                '<div class="mfp-close" style="cursor: pointer;">\327</div>'
        },
        header: {
            classSelector: 'wBasketTTL',
            appendto: '.wBasketHead',
            markup: '%%headtitle%%'
        },
        footer: {
            classSelector: 'wb_footer',
            appendto: '.wBasketFooter',
            markup: '' +
                '<div class="tar wb_footer_tot">'+
                    '<div class="wb_total">Итого: <span>%%total_price%%</span> грн.</div>'+
                '</div>'+
                '<div class="float-right text-center wb_footer_go">'+
                    '<div class="wb_gobasket">'+
                        '<a class="button alert" href="order.html" >ОФОРМИТЬ ЗАКАЗ</a>'+
                    '</div>'+
                '</div>'+
                '<div class="float-left text-center wb_footer_go">'+
                    '<div class="wb_goaway wbLeave">'+
                        '<a href="#" class="wb_close_init button" onclick="event.preventDefault();$.magnificPopup.close();">ПРОДОЛЖИТЬ ПОКУПКИ</a>'+
                    '</div>'+
                '</div>'
        },
        list: {
            element: 'ul',
            classSelector: 'wBasketList',
            classCss: 'product-catalog is-list-view',
            appendto: '.wBasketBody'
        },
        item: {
            element: 'li',
            classSelector: 'wb_item',
            elementDel: '.wb_del',
            classCss: 'product-catalog__item',
            elementConfirm: '.delConfirm',
            elementIncrement: '.wb_item_plus',
            elementDecrement: '.wb_item_minus',
            elementAmount: '.wb_item_amount',
            markup: ''+
                '<div class="product-catalog__item">'+
                    '<div class="wb_row">'+
                        '<input type="button" class="inputDelConfirm" />'+
                        '<div class="delConfirmBlock">'+
                            '<p>Вы точно хотите удалить товар?</p>'+
                            '<p class="wb_check">'+
                            '<label class="wb_select_label"><span>Нет</span></label>'+
                            '<label class="wb_select_label delConfirm"><span>Да</span></label>'+
                            '</p>'+
                        '</div>'+
                        '<div class="wb_del"><span title="Удалить товар">Удалить товар</span></div>'+
                    '</div>'+
                    '<div class="product-preview">'+
                        '<a class="product-preview__image-container" href="%%link%%" title="%%title%%">'+
                            '<img src="%%src%%" alt="%%title%%">'+
                        '</a>'+
                        '<div class="product-preview__top-pane">'+
                        '<a class="product-preview__name product-preview__name--large product-preview__for-list-view" href="%%link%%" title="%%title%%"><span>%%title%%</span></a>'+
                        '<div class="product-preview__compare-wrap">'+
                            '[[artikul]]'+'<span class="product-preview__code">%%artikul%%</span>'+'[[/artikul]]'+
                        '</div>'+
                    '</div>'+
                    '<div class="product-preview__bottom-pane">'+
                        '[[description]]'+
                        '<div class="product-preview__description product-preview__for-list-view">%%description%%</div>'+
                        '[[/description]]'+
                        '<span class="product-preview__price price">'+
                            '[[price_old]]'+
                            '<span class="price__old"><ins>%%price_old%%</ins> грн</span>'+
                            '[[/price_old]]'+
                            '<span class="price__current"><ins>%%price%%</ins> грн</span>'+
                        '</span>'+
                        '<div class="wb_cntrl">'+
                            '<div class="wb_price_one"><p><span>%%price%%</span> грн.</p></div>'+
                            '<div class="wb_amount_wrapp">'+
                                '<div class="wb_amount">'+
                                    '<input class="wb_item_amount" type="text" value="%%count%%">'+
                                    '<span class="wb_item_plus" data-spin="plus"></span>'+
                                    '<span class="wb_item_minus" data-spin="minus"></span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="wb_price_totl"><p><span>%%priceall%%</span> грн.</p></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'
        },
        preloader: {
            classSelector: 'wBasketPreloader',
            appendto: '.wBasket'
        }
    });

    $(document).magnificPopup({
        delegate: '.mfiB',
        callbacks: {
            parseAjax: function(mfpResponse, a, b, c, d) {
                var thisData = this.st.ajax.settings.data;
                var callerButton = $(this.currItem.el);
                mfpResponse.data = wBasket.Read(mfpResponse.data, {
                    setThisToInstance: function() {
                        wBasket.instance.callerButton = callerButton;
                        wBasket.instance.action = thisData.action;
                    }
                }, true);
            },
            elementParse: function(item) {
                var itemParam = item.el.data('param');
                this.st.ajax.settings = {
                    url: 'hidden/wbasket.php',
                    type: 'POST',
                    dataType: 'JSON',
                    data: itemParam
                };
            },
            ajaxContentAdded: function(el) {
                wBasket.Events();
            }
        },
        type: 'ajax',
        removalDelay: 300,
        mainClass: 'wb_slideRight'
    });

});