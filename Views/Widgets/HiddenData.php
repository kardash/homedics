<!-- WezomDefs -->
<!-- script disabled message -->
<noscript>
    <input id="wzmMsg_JsClose" type="checkbox" title="Закрыть">
    <div id="wzmMsg_JsInform" class="wzmMsg_Wrapp">
        <div class="wzmMsg_Text">
            <p>В Вашем браузере <strong>отключен JavaScript!</strong> Для корректной работы с сайтом необходима поддержка
                Javascript.</p>
            <p>Мы рекомендуем Вам включить использование JavaScript в настройках вашего браузера.</p>
        </div>
        <a href="http://wezom.com.ua/" target="_blank" title="Студия Wezom" class="wzmMsg_Link">
            <img src="pic/wezom-info-red.gif" width="50" height="18" alt="Студия Wezom">
        </a>
        <label for="wzmMsg_JsClose" class="wzmMsg_Close"><span>&times;</span></label>
    </div>
</noscript>

<!-- old browser detect -->
<script>
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }
    function $wzmOldInit() {
        var t = document.createElement("script");
        t.src = "http://verstka.vps.kherson.ua/sources/plugins/wold/wold.js", document.body.appendChild(t)
    }
    if(!Boolean(getCookie("wOld"))) {
        try {
            document.addEventListener("DOMContentLoaded", $wzmOldInit, !1);
            setCookie("wOld", "initialized", {path: "path=/"});
        } catch (e) {
            window.attachEvent("onload", $wzmOldInit);
            setCookie("wOld", "initialized", {path: "path=/"});
        }
    }
</script>

<!-- svg sprite -->
<script>
    window.wSpriteSvg = function (t, e, i) { var n = { ns: "http://www.w3.org/2000/svg", initialize: function (e, i) { if (this.prefix = "wSpriteSvg_" + e.id, t.localSupport) { var n = t.localStorage[this.prefix]; if (n) { var s = JSON.parse(n); this.setSprite(e, s) } else this.getJson(e, i) } else this.getJson(e, i) }, getJson: function (e, i) { var n = new XMLHttpRequest, s = this; n.open("GET", i, !0), n.setRequestHeader("Content-type", "application/json"), n.onreadystatechange = function () { if (4 == n.readyState && 200 == n.status) { var i = JSON.parse(n.responseText); s.setSprite(e, i), t.localSupport && t.localWrite(s.prefix, JSON.stringify(i)) } }, n.send() }, buildElem: function (t, i) { var n, s, r; for (n in i)for (s in i[n]) { var o = e.createElementNS(this.ns, s); for (r in i[n][s])"stops" === r ? this.buildElem(o, i[n][s][r]) : o.setAttributeNS(null, r, i[n][s][r]); t.appendChild(o) } }, setSprite: function (t, i) { for (var n in i) { var s = i[n]; console.log(); var r = e.createElementNS(this.ns, "symbol"); r.setAttributeNS(null, "id", n), r.setAttributeNS(null, "viewBox", s.viewBox), this.buildElem(r, s.symbol), s.hasOwnProperty("gradients") && this.buildElem(t, s.gradients), t.appendChild(r) } this.isDone(t, i) }, isDone: function (t, e) { }}; return n }(this, this.document);
</script>

<!-- изменить id у svg элемента -> #sprite_SiteName -->
<svg id="sprite_Homedics" xmlns="http://www.w3.org/2000/svg"
     style="height:0; width:0; visibility:hidden; position:absolute; top:0; left:0;"
     onload="wSpriteSvg.initialize(this,'<?php echo \Core\HTML::media('js/jsons/svgsprite.json');?>')"></svg>

<?php /*<div style="display: none;">
    <!-- Basket -->
    <div id="orderBasket" class="wBasket wBasketModule wb_animate">
        <div class="wBasketWrapp">
            <div class="wBasketHead">
                <div class="wBasketTTL">Корзина</div>
            </div>
            <div class="wBasketBody">
                <ul class="wBasketList" id="topCartList">
                    <?php $amount = 0; ?>
                    <?php foreach( $cart as $key => $item ): ?>
                        <?php $obj = Core\Arr::get( $item, 'obj' ); ?>
                        <?php if( $obj ): ?>
                            <li class="wb_item" data-id="<?php echo $obj->id; ?>" data-count="<?php echo Core\Arr::get($item, 'count', 1) ?>" data-price="<?php echo $obj->cost; ?>">
                                <div class="wb_li">
                                    <?php if( is_file(HOST.Core\HTML::media('/images/catalog/medium/'.$obj->image)) ): ?>
                                        <div class="wb_side">
                                            <div class="wb_img">
                                                <a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" class="wbLeave">
                                                    <img src="<?php echo Core\HTML::media('images/catalog/medium/'.$obj->image); ?>" />
                                                </a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="wb_content">
                                        <div class="wb_row">
                                            <div class="wb_del"><span title="Удалить товар">Удалить товар</span></div>
                                            <div class="wb_ttl">
                                                <a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" class="wbLeave">
                                                    <?php echo $obj->name; ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="wb_cntrl">
                                            <div class="wb_price_one"><p><span><?php echo $obj->cost; ?></span> грн.</p></div>
                                            <div class="wb_amount_wrapp">
                                                <div class="wb_amount">
                                                    <input type="text" class="editCountItem" value="<?php echo Core\Arr::get($item, 'count', 1); ?>">
                                                    <span data-spin="plus" class="editCountItem"></span>
                                                    <span data-spin="minus" class="editCountItem"></span>
                                                </div>
                                            </div>
                                            <div class="wb_price_totl"><p><span><?php echo $obj->cost * Core\Arr::get($item, 'count', 1); ?></span> грн.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php $amount += $obj->cost * Core\Arr::get($item, 'count', 1); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- ▼ итог корзины ▼ -->
            <div class="wBasketFooter">
                <div class="wb_footer">
                    <div class="tar wb_footer_tot">                                 
                        <div class="wb_total">Итого: <span id="topCartAmount"><?php echo $amount; ?></span> грн.</div>
                    </div>
                    <div class="flr wb_footer_go">
                        <div class="wb_gobasket">
                            <a href="<?php echo Core\HTML::link('cart'); ?>" class="wb_butt"><span>Оформить заказ</span></a>
                        </div>
                    </div>
                    <div class="fll wb_footer_go">
                        <div class="wb_goaway wbLeave">
                            <a href="#" class="wb_close_init wb_butt"><span>продолжить покупки</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ▼ дополнения к корзине ▼ -->
            <!-- <div class="wBasketAddons"></div> -->
        </div>                  
    </div>
    <div id="enterReg" class="animate_zoom">
        <div class="enterReg_top">
            <div class="enterBlock">
                <!-- Enter site -->
                <div class="title">Вход на сайт</div>
                <div id="entrForm" form="true" class="wForm enterBlock_form visForm" data-ajax="login">
                    <div class="wFormRow">
                        <input type="email" name="email" data-name="email" data-rule-email="true" placeholder="E-mail" data-rule-required="true" />
                        <label>E-mail</label>
                    </div>
                    <div class="wFormRow">
                        <input type="password" name="password" data-name="password" minlength="4" placeholder="Пароль" data-rule-required="true" />
                        <label>Пароль</label>
                    </div>
                    <label class="checkBlock">
                        <input type="checkbox" checked="checked" name="remember" data-name="remember" value="1" />
                        <ins></ins>
                        <p>Запомнить данные</p>
                    </label>
                    <?php if(array_key_exists('token', $_SESSION)): ?>
                        <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                    <?php endif; ?>
                    <div class="passLink" id="forget_pass">Забыли пароль?</div>
                    <div class="tar">
                        <button class="wSubmit enterReg_btn">войти</button>
                    </div>
                </div>
                <!-- Forgot password -->
                <div id="forgetForm" form="true" class="wForm enterBlock_form" data-ajax="forgot_password">
                    <div class="wFormRow">
                        <input type="email" data-name="email" name="email" data-rule-email="true" placeholder="E-mail" data-rule-required="true">
                        <label>E-mail</label>
                    </div>
                    <div class="forgetInf">
                        После отправления, в течении 5 минут к Вам на почту придут инструкции по восстановлению пароля.
                    </div>
                    <?php if(array_key_exists('token', $_SESSION)): ?>
                        <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                    <?php endif; ?>
                    <div class="passLink" id="remember_pass">Вернуться</div>
                    <div class="tar">
                        <button class="wSubmit enterReg_btn">отправить</button>
                    </div>
                </div>
            </div>
            <!-- Registration -->
            <div form="true" class="wForm regBlock " data-ajax="registration">
                <div class="title">Новый пользователь</div>
                <div class="wFormRow">
                    <input type="text" data-name="email" name="email" data-rule-email="true" placeholder="E-mail" data-rule-required="true" />
                    <label>E-mail</label>
                </div>
                <div class="wFormRow">
                    <input type="password" data-name="password" name="password" minlength="true" placeholder="Пароль" data-rule-required="true" />
                    <label>Пароль</label>
                </div>
                <?php if(array_key_exists('token', $_SESSION)): ?>
                    <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                <?php endif; ?>
                <label class="checkBlock">
                    <input type="checkbox" data-name="agree" name="agree" data-rule-required="true" value="1" />
                    <ins></ins>
                    <p>Я согласен с условиями использования и обработку моих персональных данных</p>
                </label>
                <div class="tar">
                    <button class="wSubmit enterReg_btn">зарегистрироваться</button>
                </div>
            </div>
        </div>
        <!-- Enter by social networks -->
        <div class="socEnter">
            <p>Вход через</p>
            <div class="socLinkEnter">
                <div id="uLogin" data-ulogin="display=small;fields=first_name,last_name,email;providers=vkontakte,facebook,odnoklassniki,mailru;hidden=;redirect_uri=http%3A%2F%2F<?php echo $_SERVER['HTTP_HOST']; ?>%2Faccount%2Flogin-by-social-network"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!-- Callback -->
    <div id="enterReg2" class="animate_zoom">
        <div class="enterReg_top">
            <div form="true" class="wForm regBlock" data-ajax="callback">
                <div class="title">Заказ звонка</div>
                <div class="wFormRow">
                    <input type="text" data-name="name" name="name" data-rule-bykvu="true" placeholder="Имя" data-rule-minlength="2" data-rule-required="true">
                    <label>Имя</label>
                </div>
                <div class="wFormRow">
                    <input type="tel" class="tel" data-name="phone" name="phone" data-rule-phoneUA="true" maxlength="19" minlength="19" placeholder="Телефон" data-rule-required="true">
                    <label>Телефон</label>
                </div>
                <?php if(array_key_exists('token', $_SESSION)): ?>
                    <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                <?php endif; ?>
                <div class="tar">
                    <button class="wSubmit enterReg_btn">заказать звонок</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Quick order -->
    <div id="enterReg5" class="animate_zoom">
        <div class="enterReg_top">
            <div form="true" class="wForm regBlock" data-ajax="order_simple">
                <div class="title">Быстрый заказ</div>
                <div class="wFormRow">
                    <input type="tel" class="tel" data-name="phone" name="phone" data-rule-phoneUA="true" maxlength="19" minlength="19" placeholder="Телефон" data-rule-required="true">
                    <label>Телефон</label>
                </div>
                <?php if(array_key_exists('token', $_SESSION)): ?>
                    <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                <?php endif; ?>
                <input type="hidden" data-name="id" id="idFastOrder" name="id" value="<?php echo Core\Route::param('id'); ?>" />
                <div class="tar">
                    <button class="wSubmit enterReg_btn">отправить</button>
                </div>
            </div>
        </div>
    </div>
</div>*/?>
