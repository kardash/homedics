<?php /*<header class="wHeader">
    <div class="wSize">
        <div class="head_top">
            <div class="fll">
                <ul>
                    <li><a href="<?php echo Core\HTML::link(); ?>"><div class="gl"></div></a></li>
                    <?php foreach ( $contentMenu as $obj ): ?>
                        <li><a href="<?php echo Core\HTML::link($obj->url); ?>"><?php echo $obj->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="flr">
                <div class="block_p">
                    <a href="tel:<?php echo Core\Config::get( 'static.phone' ); ?>?call" class="head_phone"><?php echo Core\Config::get( 'static.phone' ); ?></a>
                    <a href="#enterReg2" class="call_back enterReg2"><span>ОБРАТНЫЙ ЗВОНОК</span></a>
                </div>
            </div>
            <div class="flc">
                <!-- <a href="<?php // echo Core\HTML::link(); ?>"><img src="<?php // echo Core\HTML::media('pic/logo.png'); ?>" alt=""></a> -->
            </div>
        </div>
        <div class="head_center">
            <div class="fll">
                <ul class="soc_seti">
                    <li><a href="<?php echo Core\Config::get( 'socials.vk'); ?>" class="circle_seti" target="_blank"><span class="img_seti"></span><div class="name_seti"></div><div class="name_seti"></div></a></li>
                    <li><a href="<?php echo Core\Config::get( 'socials.fb'); ?>" class="circle_seti" target="_blank"><span class="img_seti"></span><div class="name_seti"></div><div class="name_seti"></div></a></li>
                    <li><a href="<?php echo Core\Config::get( 'socials.instagram'); ?>" class="circle_seti" target="_blank"><span class="img_seti"></span><div class="name_seti"></div><div class="name_seti"></div></a></li>
                </ul>
            </div>
            <div class="flr">
                <?php if ( !$user ): ?>
                    <a href="#enterReg" class="enter enterReg"><span>Вход</span></a>
                <?php else: ?>
                    <a href="<?php echo Core\HTML::link('account'); ?>" class="basket enter"><span>Кабинет</span></a>
                    <a href="<?php echo Core\HTML::link('account/logout'); ?>" class="basket"><span>Выход</span></a>
                <?php endif ?>
                <a href="<?php echo Core\HTML::link('cart'); ?>" class="basket"><span>Корзина</span></a>
                <a href="#orderBasket" class="basket_img wb_edit_init wb_butt"><div class="paket"></div><span class="paket_in"></span><span id="topCartCount"><?php echo $countItemsInTheCart; ?></span></a>
            </div>
            <?php echo Core\Widgets::get( 'Info' ); ?>
        </div>
        <div class="head_bot">
            <?php echo Core\Widgets::get( 'CatalogMenuTop' ); ?>
            <div class="lupa"></div>
            <div class="poisk_block">
                <form action="<?php echo Core\HTML::link('search'); ?>" method="GET">
                    <input type="text" name="query" placeholder="Поиск по сайту">
                    <input type="submit" value="искать">
                </form>
            </div>
        </div>
    </div>
</header>*/?>
<header class="page-header">
  <div class="page-header__mobile-pane hide-for-xlarge">
	<div class="row">
	  <a class="logo logo--mobile" href="/" title="Homedics">
		<img class="logo__image" src="<?php echo \Core\HTML::media('pic/logo.png');?>" alt="Homedics">
	  </a>
	  <div class="float-right">
		<div class="user-features user-features--mobile">
		  <a class="user-features__button" href="compare.html" title="Сравнить">
			<span class="figure">
			  <i class="figure__icon">
				<svg>
				  <use xlink:href="#icon_weights"></use>
				</svg>
			  </i>
			  <span class="figure__badge badge js-compare-count">0</span>
			</span>
			<span class="user-features__caption">Сравнить</span>
		  </a>
		  <a class="user-features__button" href="favorite.html" title="Избранное">
			<span class="figure">
			  <i class="figure__icon">
				<svg>
				  <use xlink:href="#icon_heart"></use>
				</svg>
			  </i>
			  <span class="figure__badge alert badge js-favorite-count">0</span>
			</span>
			<span class="user-features__caption">Избранное</span>
		  </a>
		  <a class="user-features__button cart-button mfiB" data-param='{"action": "default"}' href="#">
			<span class="figure">
			  <i class="figure__icon">
				<svg>
				  <use xlink:href="#icon_cart"></use>
				</svg>
			  </i>
			  <span class="figure__badge badge js-basket-quantity">0</span>
			</span>
			<span class="user-features__caption">Корзина</span>
		  </a>
		  <button class="user-features__button" title="Меню" data-toggle="offCanvas">
			<span class="figure">
			  <i class="figure__icon">
				<svg>
				  <use xlink:href="#icon_burger"></use>
				</svg>
			  </i>
			</span>
			<span class="user-features__caption">Меню</span>
		  </button>
		</div>
	  </div>
	</div>
  </div>
  <div class="show-for-xlarge">
	<div class="page-header__top-pane">
	  <div class="row">
		<div class="float-left">
		  <ul class="menu simple page-header__top-pane-menu">
			<li><a href="contacts.html" title="О компании">О компании</a></li>
			<li><a href="articles.html" title="Статьи">Статьи</a></li>
			<li><a href="testimonials.html" title="Отзывы">Отзывы</a></li>
			<li><a href="warranty.html" title="Гарантия и сертификаты">Гарантия и сертификаты</a></li>
			<li><a href="delivery.html" title="Доставка и оплата">Доставка и оплата</a></li>
			<li><a href="faq.html" title="Вопрос-Ответ">Вопрос-Ответ</a></li>
			<li><a href="contacts.html" title="Контакты">Контакты</a></li>
		  </ul>
		</div>
		<div class="float-right">
		  <ul class="menu simple page-header__top-pane-menu page-header__top-pane-menu--right">
			<li><a class="mfiA" href="#" title="Вход/Регистрация" data-url="hidden/auth.php" data-param='{"id": "login"}'>Вход/Регистрация</a></li>
			<!-- Ссылка на личный кабинет для авторизированного пользователя -->
			<!--<li><a class="ellipsis" style="max-width: 18rem;" href="account.html" title="Личный кабинет" data-url="hidden/auth.php">Андрей Степанов</a></li>-->
			<li>
			  <div class="dropdown-select dropdown-select--language">
				<div class="dropdown-select__header">
				  <i class="dropdown-select__icon">
					<svg>
					  <use xlink:href="#icon_global"></use>
					</svg>
				  </i>
				  <span class="show-for-sr">Выбор языка</span>
				</div>
				<div class="dropdown-select__body-holder">
				  <div class="dropdown-select__body">
					<div class="languages">
					  <div class="languages__list">
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/la.png" alt="">
							<span class="languages__name">América Latina</span>
							<span class="languages__sub-name">Español</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/netherlands.png" alt="">
							<span class="languages__name">Nederland</span>
							<span class="languages__sub-name">Nederlands</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/australia.png" alt="">
							<span class="languages__name">Australia</span>
							<span class="languages__sub-name">English</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/austria.png" alt="">
							<span class="languages__name">Österreich</span>
							<span class="languages__sub-name">Deutsch</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/belgium.png" alt="">
							<span class="languages__name">Belgique</span>
							<span class="languages__sub-name">Français</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/philippines.png" alt="">
							<span class="languages__name">Philippines</span>
							<span class="languages__sub-name">English</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/belgium.png" alt="">
							<span class="languages__name">België</span>
							<span class="languages__sub-name">Nederlands</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/poland.png" alt="">
							<span class="languages__name">Polska</span>
							<span class="languages__sub-name">Polski</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/brazil.png" alt="">
							<span class="languages__name">Brasil</span>
							<span class="languages__sub-name">Português</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/russia.png" alt="">
							<span class="languages__name">Россия</span>
							<span class="languages__sub-name">Русский</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/canada.png" alt="">
							<span class="languages__name">Canada</span>
							<span class="languages__sub-name">English</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/switzerland.png" alt="">
							<span class="languages__name">Schweiz</span>
							<span class="languages__sub-name">Deutsch</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/germany.png" alt="">
							<span class="languages__name">Deutschland</span>
							<span class="languages__sub-name">Deutsch</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/switzerland.png" alt="">
							<span class="languages__name">Suisse</span>
							<span class="languages__sub-name">Français</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/spain.png" alt="">
							<span class="languages__name">España</span>
							<span class="languages__sub-name">Español</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/switzerland.png" alt="">
							<span class="languages__name">Svizzera</span>
							<span class="languages__sub-name">Italiano</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/france.png" alt="">
							<span class="languages__name">France</span>
							<span class="languages__sub-name">Français</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/uk.png" alt="">
							<span class="languages__name">United Kingdom</span>
							<span class="languages__sub-name">English</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/italy.png" alt="">
							<span class="languages__name">Italia</span>
							<span class="languages__sub-name">Italiano</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/usa.png" alt="">
							<span class="languages__name">United States</span>
							<span class="languages__sub-name">English</span>
						  </a>
						</div>
						<div class="languages__item">
						  <a class="languages__link" href="#">
							<img class="languages__flag" src="pic/flags/mexico.png" alt="">
							<span class="languages__name">México</span>
							<span class="languages__sub-name">Español</span>
						  </a>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</li>
		  </ul>
		</div>
	  </div>
	</div>
	<div class="page-header__middle-pane">
	  <div class="row">
		<div class="float-left">
		  <a class="logo page-header__logo" href="/" title="Homedics">
			<img src="<?php echo \Core\HTML::media('pic/logo.png');?>" alt="Homedics" class="logo__image">
			<div class="logo__text">
			  <p>№1 Лидер массажного <br> оборудования и <br> товаров для красоты</p>
			</div>
		  </a>
		</div>
		<div class="float-right">
		  <div class="dropdown-select dropdown-select--for-tels">
			<div class="dropdown-select__header">
				<a class="link link--tel" href="tel:+380979193573" title="+38 097 91 93 573">+38 097 91 93 573</a>
			</div>
			<div class="dropdown-select__body-holder">
			  <div class="dropdown-select__body">
				<ul class="menu vertical">
				  <li><a class="link link--tel" href="tel:+380932638748" title="+38 093 26 38 748"><img src="pic/mob_kyivstar.jpg" alt="">+38 093 26 38 748</a>
				  </li>
				  <li><a class="link link--tel" href="tel:+380932638748" title="+38 093 26 38 748"><img src="pic/mob_life.jpg" alt="">+38 093 26 38 748</a>
				  </li>
				  <li><a class="link link--tel" href="tel:+380508232565" title="+38 050 82 32 565"><img src="pic/mob_mts.jpg" alt="">+38 050 82 32 565</a>
				  </li>
				  <li><a class="link link--tel" href="tel:+380508232565" title="+38 050 82 32 565"><img src="pic/phone.jpg" alt="">+38 050 82 32 565</a>
				  </li>
				</ul>
				<p>&nbsp;</p>
				  <button class="button small alert expand mfiA" data-url="hidden/callback.php" title="Укажите Ваш номер телефона и мы свяжемся с Вами">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</button>
				<p>&nbsp;</p>
			  </div>
			</div>
			<div class="dropdown-select__footer"><span>Показать другие номера</span></div>
		  </div>
		  <div class="user-features user-features--padding-top">
			<a class="user-features__button" href="compare.html" title="Сравнить">
			  <span class="figure">
				<i class="figure__icon">
				  <svg>
					<use xlink:href="#icon_weights"></use>
				  </svg>
				</i>
				<span class="figure__badge badge js-compare-count">0</span>
			  </span>
			</a>
			<a class="user-features__button" href="favorite.html" title="Избранное">
			  <span class="figure">
				<i class="figure__icon">
				  <svg>
					<use xlink:href="#icon_heart"></use>
				  </svg>
				</i>
			  <span class="figure__badge alert badge js-favorite-count">0</span>
			</span>
			</a>
			<a class="user-features__button cart-button mfiB" data-param='{"action": "default"}' href="#">
			<span class="figure cart-button__figure">
			  <i class="figure__icon">
				<svg>
				  <use xlink:href="#icon_cart"></use>
				</svg>
			  </i>
			  <span class="figure__badge badge js-basket-quantity">0</span>
			</span>
			  <span class="cart-button__title">Корзина</span>
			<span class="cart-button__sum-wrap"><span class="cart-button__sum js-basket-total">0</span>&nbsp;<span
					class="cart-button__currency">грн</span></span>
			</a>
		  </div>
		</div>
	  </div>
	</div>
	<div class="page-header__bottom-pane">
	  <div class="row">
		<div class="float-left">
		  <div class="catalog-menu">
			<ul class="catalog-menu__list">
			  <li class="catalog-menu__item">
				<div class="dropdown-select dropdown-select--catalog-menu">
				  <a class="dropdown-select__header" href="#" title="Массаж">МАССАЖ</a>
				  <div class="dropdown-select__body-holder">
					<div class="dropdown-select__body submenu js-submenu">
					  <div class="row">
						<div class="column small-6">
						  <ul class="submenu__menu menu vertical">
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Эпиляторы">Эпиляторы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Косметические зеркала">Косметические зеркала</a></li>
						  </ul>
						</div>
						<div class="column small-6">
						  <a class="submenu__image-container js-submenu-img-link" href="catalog.html" title="Эпиляторы"><img
								  class="js-submenu-img" src="pic/img-submenu-1.jpg" alt=""></a>
						  <div class="submenu__main-link-wrap"><a class="js-submenu-main-link" href="catalog.html"
																  title="Эпиляторы">ВСЕ ТОВАРЫ</a></div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </li>
			  <li class="catalog-menu__item">
				<div class="dropdown-select dropdown-select--catalog-menu">
				  <a class="dropdown-select__header" href="#" title="Красота">КРАСОТА</a>
				  <div class="dropdown-select__body-holder">
					<div class="dropdown-select__body submenu js-submenu">
					  <div class="row">
						<div class="column small-6">
						  <ul class="submenu__menu menu vertical">
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Эпиляторы">Эпиляторы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Косметические зеркала">Косметические зеркала</a></li>
						  </ul>
						</div>
						<div class="column small-6">
						  <a class="submenu__image-container js-submenu-img-link" href="catalog.html" title="Эпиляторы"><img
								  class="js-submenu-img" src="pic/img-submenu-1.jpg" alt=""></a>
						  <div class="submenu__main-link-wrap"><a class="js-submenu-main-link" href="catalog.html"
																  title="Эпиляторы">ВСЕ ТОВАРЫ</a></div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </li>
			  <li class="catalog-menu__item">
				<div class="dropdown-select dropdown-select--catalog-menu">
				  <a class="dropdown-select__header" href="#" title="Здоровье">ЗДОРОВЬЕ</a>
				  <div class="dropdown-select__body-holder">
					<div class="dropdown-select__body submenu js-submenu">
					  <div class="row">
						<div class="column small-6">
						  <ul class="submenu__menu menu vertical">
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Эпиляторы">Эпиляторы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Косметические зеркала">Косметические зеркала</a></li>
						  </ul>
						</div>
						<div class="column small-6">
						  <a class="submenu__image-container js-submenu-img-link" href="catalog.html" title="Эпиляторы"><img
								  class="js-submenu-img" src="pic/img-submenu-1.jpg" alt=""></a>
						  <div class="submenu__main-link-wrap"><a class="js-submenu-main-link" href="catalog.html"
																  title="Эпиляторы">ВСЕ ТОВАРЫ</a></div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </li>
			  <li class="catalog-menu__item">
				<div class="dropdown-select dropdown-select--catalog-menu">
				  <a class="dropdown-select__header" href="#" title="Аксессуары">АКСЕССУАРЫ</a>
				  <div class="dropdown-select__body-holder">
					<div class="dropdown-select__body submenu js-submenu">
					  <div class="row">
						<div class="column small-6">
						  <ul class="submenu__menu menu vertical">
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Эпиляторы">Эпиляторы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-lot-1.jpg" href="catalog.html"
								   title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-1.jpg" href="catalog.html"
								   title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a class="js-submenu-link" data-img-src="pic/img-submenu-2.jpg" href="catalog.html"
								   title="Косметические зеркала">Косметические зеркала</a></li>
						  </ul>
						</div>
						<div class="column small-6">
						  <a class="submenu__image-container js-submenu-img-link" href="catalog.html" title="Эпиляторы"><img
								  class="js-submenu-img" src="pic/img-submenu-1.jpg" alt=""></a>
						  <div class="submenu__main-link-wrap"><a class="js-submenu-main-link" href="catalog.html"
																  title="Эпиляторы">ВСЕ ТОВАРЫ</a></div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </li>
			  <li class="catalog-menu__item">
				<div class="dropdown-select dropdown-select--catalog-menu">
				  <a class="dropdown-select__header color-alert" href="#" title="Акции">% АКЦИИ</a>
				</div>
			  </li>
			</ul>
		  </div>
		</div>
		<div class="float-right">
		  <div class="wForm wFormDef wFormDef--search page-header__search-form" data-form="true">
			<label class="wFormInput">
			  <input class="wInput" type="search" name="header_search" placeholder="ПОИСК"
					 data-rule-minlength="3" data-msg-minlength="Введите не менее 3 символов">
			</label>
			<button class="wSubmit" type="submit">
			  <svg>
				<use xlink:href="#icon_search"></use>
			  </svg>
			</button>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</header>