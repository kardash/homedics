<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body>
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
	<div class="off-canvas-wrapper">
		<div id="seoTxt" class="seo-txt">
			<div class="wSize">
				<div class="wTxt">
				   <?php echo $_content; ?>
				</div>
			</div>
		</div><!-- /seoTxt -->
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<div id="offCanvas" class="off-canvas mobile-ofc position-left" data-off-canvas data-auto-focus="false" data-force-top="true">
				<button class="close-button" data-close aria-label="Close menu" type="button">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="mobile-ofc__menu-title"><span>МЕНЮ</span></div>
				<div class="wForm wFormDef wFormDef--search" data-form="true">
					<label class="wFormInput">
						<input class="wInput" type="search" required name="off_canvas_search" placeholder="ПОИСК"
							   data-rule-minlength="3" data-msg-minlength="Введите не менее 3 символов" data-msg-required="Пожалуйста, заполните поле!">
					</label>
					<button class="wSubmit" type="submit">
						<svg>
							<use xlink:href="#icon_search"></use>
						</svg>
					</button>
				</div>
				<div class="mobile-ofc__devision-title"><span>Личный кабинет</span></div>
				<ul class="menu vertical">
					<!--Меню для неавторизованного пользователя-->
					<li><a class="mfiA" href="#" title="О компании" data-url="hidden/auth.php" data-param='{"id": "login"}'>Вход/регистрация</a></li>
					<!--Меню для авторизованного пользователя-->
					<!--<li><a href="account.html" title="Мой аккаунт">Мой аккаунт</a></li>
					<li><a href="account-history.html" title="История заказов">История заказов</a></li>
					<li><a href="account-favorite.html" title="Список желаний">Список желаний</a></li>-->
				</ul>
				<div class="mobile-ofc__devision-title"><span>Навигация</span></div>
				<ul class="menu vertical" data-accordion-menu>
					<li><a href="#" title="О компании">О компании</a></li>
					<li><a href="#" title="Статьи">Статьи</a></li>
					<li><a href="#" title="Отзывы">Отзывы</a></li>
					<li><a href="#" title="Гарантия и сертификаты">Гарантия и сертификаты</a></li>
					<li><a href="#" title="Доставка и оплата">Доставка и оплата</a></li>
					<li><a href="#" title="Вопрос-Ответ">Вопрос-Ответ</a></li>
					<li>
						<a href="#" title="Контакты">Контакты</a>
						<ul class="menu vertical nested">
							<li class="menu-text color-primary">Контактные номера телефонов:</li>
							<li><a href="tel:+380932638748" title="+38 093 26 38 748">+38 093 26 38 748</a></li>
							<li><a href="tel:+380932638748" title="+38 093 26 38 748">+38 093 26 38 748</a></li>
							<li><a href="tel:+380508232565" title="+38 050 82 32 565">+38 050 82 32 565</a></li>
							<li><a href="tel:+380508232565" title="+38 050 82 32 565">+38 050 82 32 565</a></li>
							<li><a href="#" class="mfiA" data-url="hidden/callback.php" title="Заказать обратный звонок">Заказать обратный звонок</a></li>
							<li class="menu-text color-primary">Наш адрес:</li>
							<li class="menu-text" style="padding-left: 2rem;">Центральный офис:г. Киев, пр-т Московский, 8/16</li>
							<li><a href="#" class="color-warning" style="padding-left: 1rem;"><b>Перейти на страницу "Контакты" &rarr;</b></a></li>
						</ul>
					</li>
				</ul>
				<div class="mobile-ofc__devision-title"><span>Каталог</span></div>
				<ul class="menu vertical" data-accordion-menu data-multi-open="false">
					<li>
						<a href="catalog.html" title="Массаж">Массаж</a>
						<ul class="menu vertical nested">
							<li><a href="catalog.html" title="Эпиляторы">Эпиляторы</a></li>
							<li><a href="catalog.html" title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a href="catalog.html" title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a href="catalog.html" title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a href="catalog.html" title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a href="catalog.html" title="Косметические зеркала">Косметические зеркала</a></li>
							<li><a href="catalog.html" title="Все товары">Все товары</a></li>
						</ul>
					</li>
					<li>
						<a href="catalog.html" title="Массаж">Красота</a>
						<ul class="menu vertical nested">
							<li><a href="catalog.html" title="Эпиляторы">Эпиляторы</a></li>
							<li><a href="catalog.html" title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a href="catalog.html" title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a href="catalog.html" title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a href="catalog.html" title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a href="catalog.html" title="Косметические зеркала">Косметические зеркала</a></li>
							<li><a href="catalog.html" title="Все товары">Все товары</a></li>
						</ul>
					</li>
					<li>
						<a href="catalog.html" title="Здоровье">Здоровье</a>
						<ul class="menu vertical nested">
							<li><a href="catalog.html" title="Эпиляторы">Эпиляторы</a></li>
							<li><a href="catalog.html" title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a href="catalog.html" title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a href="catalog.html" title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a href="catalog.html" title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a href="catalog.html" title="Косметические зеркала">Косметические зеркала</a></li>
							<li><a href="catalog.html" title="Все товары">Все товары</a></li>
						</ul>
					</li>
					<li>
						<a href="catalog.html" title="Аксессуары">Аксессуары</a>
						<ul class="menu vertical nested">
							<li><a href="catalog.html" title="Эпиляторы">Эпиляторы</a></li>
							<li><a href="catalog.html" title="Аппараты для похудения">Аппараты для похудения</a></li>
							<li><a href="catalog.html" title="Маникюрно-педикюрные наборы">Маникюрно-педикюрные наборы</a></li>
							<li><a href="catalog.html" title="Гидромассажные ванночки">Гидромассажные ванночки</a></li>
							<li><a href="catalog.html" title="Щетки для пилинга">Щетки для пилинга</a></li>
							<li><a href="catalog.html" title="Косметические зеркала">Косметические зеркала</a></li>
							<li><a href="catalog.html" title="Все товары">Все товары</a></li>
						</ul>
					</li>
					<li><a href="catalog.html" title="% Акции">% Акции</a></li>
				</ul>
				<div class="mobile-ofc__devision-title"><span>Языки</span><i>
					<svg>
						<use xlink:href="#icon_global"></use>
					</svg>
				</i></div>
				<ul class="menu vertical" data-accordion-menu>
					<li>
						<a href="#">Список доступных языков</a>
						<ul class="menu vertical nested">
							<li><a href="#">América Latina <span class="color-primary">Español</span></a></li>
							<li><a href="#">Australia <span class="color-primary">English</span></a></li>
							<li><a href="#">Belgique <span class="color-primary">Français</span></a></li>
							<li><a href="#">België <span class="color-primary">Nederlands</span></a></li>
							<li><a href="#">Brasil <span class="color-primary">Português</span></a></li>
							<li><a href="#">Canada <span class="color-primary">English</span></a></li>
							<li><a href="#">Deutschland <span class="color-primary">Deutsch</span></a></li>
							<li><a href="#">España <span class="color-primary">Español</span></a></li>
							<li><a href="#">France <span class="color-primary">Français</span></a></li>
							<li><a href="#">Italia <span class="color-primary">Italiano</span></a></li>
							<li><a href="#">México <span class="color-primary">Español</span></a></li>
							<li><a href="#">Nederland <span class="color-primary">Nederlands</span></a></li>
							<li><a href="#">Österreich <span class="color-primary">Deutsch</span></a></li>
							<li><a href="#">Philippines <span class="color-primary">English</span></a></li>
							<li><a href="#">Polska <span class="color-primary">Polski</span></a></li>
							<li><a href="#">Россия <span class="color-primary">Русский</span></a></li>
							<li><a href="#">Schweiz <span class="color-primary">Deutsch</span></a></li>
							<li><a href="#">Suisse <span class="color-primary">Français</span></a></li>
							<li><a href="#">Svizzera <span class="color-primary">Italiano</span></a></li>
							<li><a href="#">United Kingdom <span class="color-primary">English</span></a></li>
							<li><a href="#">United States <span class="color-primary">English</span></a></li>
						</ul>
					</li>
				</ul>
				<div class="mobile-ofc__devision-title"><span>Мы в социальных сетях</span></div>
				<div class="mobile-ofc__socials socials text-center">
					<a href="#" title="facebook" class="socials__link"><i class="socials__icon fill-hover-fb">
						<svg>
							<use xlink:href="#icon_facebook"></use>
						</svg>
					</i></a>
					<a href="#" title="twitter" class="socials__link"><i class="socials__icon fill-hover-tt">
						<svg>
							<use xlink:href="#icon_twitter"></use>
						</svg>
					</i></a>
					<a href="#" title="youtube" class="socials__link"><i class="socials__icon fill-hover-yt">
						<svg>
							<use xlink:href="#icon_youtube"></use>
						</svg>
					</i></a>
					<a href="#" title="pinterest" class="socials__link"><i class="socials__icon fill-hover-pt">
						<svg>
							<use xlink:href="#icon_pinterest"></use>
						</svg>
					</i></a>
					<a href="#" title="instagram" class="socials__link"><i class="socials__icon fill-hover-fb">
						<svg>
							<use xlink:href="#icon_instagram"></use>
						</svg>
					</i></a>
					<a href="#" title="google+" class="socials__link"><i class="socials__icon fill-hover-gp">
						<svg>
							<use xlink:href="#icon_google_plus"></use>
						</svg>
					</i></a>
					<a href="#" title="blog" class="socials__link socials__link--blog">
						<span class="font-size-tiny font-family-georgia"><em>наш</em></span>
						<span class="font-size-small font-family-gothic">- <b>БЛОГ</b> -</span>
					</a>
				</div>
			</div>
			<div class="off-canvas-content" data-off-canvas-content>
				<div class="page-wrapper">
					 <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
				
					<div class="page-container">
						
						<div class="carousel carousel--main show-for-large js-carousel" data-carousel-autoplay="true" data-carousel-speed="600" data-breakpoint="large">
						  <div class="carousel__inner">
							<div class="carousel__track js-carousel-track">
							  <div class="carousel__item">
								<a href="item.html">
								  <img src="" alt="" data-interchange="[pic/img-slider.jpg, medium]">
								</a>
							  </div>
							  <div class="carousel__item">
								<a href="item.html">
								  <img src="" alt="" data-interchange="[pic/img-slider.jpg, medium]">
								</a>
							  </div>
							  <div class="carousel__item">
								<a href="item.html">
								  <img src="" alt="" data-interchange="[pic/img-slider.jpg, medium]">
								</a>
							  </div>
							</div>
							<div class="carousel__arrows">
							  <div class="carousel__arrows-track">
								<button class="carousel__arrow carousel__arrow--prev js-carousel-prev" type="button" title="Назад"><i></i><span
										class="show-for-sr">Назад</span></button>
								<button class="carousel__arrow carousel__arrow--next js-carousel-next" type="button" title="Вперёд"><i></i><span
										class="show-for-sr">Вперёд</span></button>
							  </div>
							</div>
						  </div>
						</div> <!-- /main-carousel -->
						<div class="page-section">
						  <div class="quick-registration">
							  <div class="row">
								  <div class="quick-registration__inner">
									  <div class="quick-registration__title">
										  <p class="font-family-gothic" style="font-size: 2.2rem">ЗАРЕГИСТРИРУЙТЕСЬ И ПОЛУЧИТЕ СКИДКУ!</p>
										  <p class="font-family-georgia color-white"><em>Зарегистрируйтесь сегодня и получите код купона со
											  скидкой <span style="font-size: 3rem">10<span
													  style="font-size: 2rem">%</span></span>.</em></p>
									  </div>
									  <div class="quick-registration__form wForm wFormDef" data-form="true">
										  <div class="wFormInput">
											  <input type="email" class="wInput" required data-rule-email="true"
													 data-msg-email="Пожалуйста, введите корректный адрес электронной почты!"
													 data-msg-required="Пожалуйста, заполните поле!" placeholder="your_email@example.com">
											  <div class="inpInfo">email</div>
										  </div>
										  <button class="wSubmit button alert">РЕГИСТРАЦИЯ</button>
									  </div>
								  </div>
							  </div>
						  </div>
						</div> <!-- /page-section -->
						<div class="page-section promo-section">
						  <div class="row xlarge-collapse">
							<div class="column small-12 xlarge-9">
							  <div class="promo-container">
								<a class="promo promo--large color-black" href="catalog.html" style="background-image: url('pic/bg-promo-2.jpg')">
								  <div class="promo__inner text-center">
									<p class="promo__title"><em>подбери свой</em></p>
									<p class="promo__sub-title">ЛЮБИМЫЙ МАССАЖЕР</p>
									<p><span class="button alert">ПЕРЕЙТИ В КАТАЛОГ</span></p>
								  </div>
								</a>
								<a class="promo color-white" href="catalog.html" style="background-image: url('pic/bg-promo-1.jpg')">
								  <div class="promo__inner">
									<p><span class="promo__link">КРАСОТА</span></p>
								  </div>
								</a>
								<a class="promo color-black" href="catalog.html" style="background-image: url('pic/bg-promo-4-4.jpg')">
								  <div class="promo__inner">
									<p><span class="promo__link">ТОВАРЫ ДЛЯ<br> ЗДОРОВЬЯ</span></p>
								  </div>
								</a>
							  </div>
							  <div class="promo-container promo-container--up-to-3">
								<a href="catalog.html" class="promo promo--product color-white" style="background-image: url('pic/bg-promo-5.jpg')">
								  <span class="product-label product-label--succes">НОВИНКА</span>
								  <div class="promo__inner">
									<p class="promo__title">Ультразвуковой<br> Увлажнитель и<br> освежитель Mist<br> Humidifier</p>
								  </div>
								</a>
								<a href="catalog.html" class="promo promo--framed color-black">
								  <div class="promo__inner text-center">
									<p class="promo__title"><em>Акции и<br> Скидки<br> до</em></p>
									<p class="promo__sub-title">40%</p>
									<span class="promo__link">Подробнее</span>
								  </div>
								</a>
								<a href="catalog.html" class="promo color-white" style="background-image: url('pic/bg-promo-6.jpg')">
								  <span class="promo__inner">
									<p><span class="promo__link">Запчасти &<br> аксессуары</span></p>
								  </span>
								</a>
							  </div>
							</div>
							<div class="column small-12 xlarge-3 promo-section__right-column">
							  <div class="promo-container">
								<a href="catalog.html" class="promo promo--diamond color-black" style="background-image: url('pic/bg-promo-3.jpg')">
								  <i class="promo__svg">
									<svg>
									  <use xlink:href="#icon_diamond"></use>
									</svg>
								  </i>
								  <div class="promo__inner text-center">
									<p class="promo__title"><em>полезные<br> статьи</em></p>
									<p><span class="promo__link">читать</span></p>
								  </div>
								</a>
								<div class="carousel carousel--promo js-carousel">
								  <div class="carousel__header">
									<span>Хиты продаж</span>
								  </div>
								  <div class="carousel__inner">
									<div class="carousel__track js-carousel-track">
									  <div class="carousel__item">
										<div class="carousel__product-preview product-preview">
										  <a class="product-preview__image-container" href="item.html" title="Портативный увлажнитель Mist Humidifier">
											<img src="pic/img-lot-1.jpg" alt="Портативный увлажнитель Mist Humidifier">
										  </a>
										  <div class="product-preview__top-pane">
											<a class="product-preview__name product-preview__name--large product-preview__for-list-view" href="item.html"
											   title="Портативный увлажнитель Mist Humidifier"><span>Портативный увлажнитель Mist Humidifier</span></a>
											<a class="product-preview__rating rating product-preview__for-list-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="4">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>2</ins> Отзыва</span>
											</a>
											<div class="product-preview__compare-wrap">
											  <span class="product-preview__code">UHE-CM15</span>
											  <button class="product-preview__compare switchable-button js-compare-add" data-product-id="001" type="button" title="Добавить товар к сравненю"><span class="switchable-button__text" data-default="+ Сравнить" data-added="В сравнении"></span></button>
											</div>
										  </div>
										  <div class="product-preview__bottom-pane">
											<a class="product-preview__name product-preview__for-tile-view" href="item.html" title="Портативный увлажнитель Mist Humidifier"><span>Портативный увлажнитель Mist Humidifier</span></a>
											<div class="product-preview__description product-preview__for-list-view">
											  <p>HoMedics® Портативный массаж спины подушки обеспечивает бодрящий массаж вибрации для всей спине. В комплекте с комплексной системой управления, позволяющей выбирать между высоким или низким настроек массажа, задняя массажные подушки идеально подходит для любого случая.</p>
											  <p><a class="button primary tiny" href="item.html" title="Портативный увлажнитель Mist Humidifier"><span>ПОДРОБНЕЕ</span></a></p>
											</div>
											<a class="product-preview__rating rating product-preview__for-tile-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="4">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>2</ins> Отзыва</span>
											</a>
											<span class="product-preview__price price">
											  <span class="price__old"><ins>840</ins> грн</span>
											  <span class="price__current"><ins>760</ins> грн</span>
											</span>
											<button class="product-preview__button button button--buy button--gray expanded mfiB" type="button" data-param='{"action": "new", "id": 123}' title="Добавить товар в корзину"><span>КУПИТЬ</span> <i>
											  <svg>
												<use xlink:href="#icon_plus"></use>
											  </svg>
											</i></button>
											<a class="product-preview__button product-preview__for-tile-view button primary tiny" href="item.html" title="Портативный увлажнитель Mist Humidifier"><span>ПОДРОБНЕЕ</span></a>
										  </div>
										</div>
									  </div>
									  <div class="carousel__item">
										<div class="carousel__product-preview product-preview">
										  <a class="product-preview__image-container" href="item.html" title="Atlas Vibration Acu-Node Massager">
											<img src="pic/img-lot-2.jpg" alt="Atlas Vibration Acu-Node Massager">
										  </a>
										  <div class="product-preview__top-pane">
											<a class="product-preview__name product-preview__name--large product-preview__for-list-view" href="item.html"
											   title="Atlas Vibration Acu-Node Massager"><span>Atlas Vibration Acu-Node Massager</span></a>
											<a class="product-preview__rating rating product-preview__for-list-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="4">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>24</ins> Отзыва</span>
											</a>
											<div class="product-preview__compare-wrap">
											  <span class="product-preview__code">UHE-CM25</span>
											  <button class="product-preview__compare switchable-button js-compare-add" data-product-id="002" type="button" title="Добавить товар к сравненю"><span class="switchable-button__text" data-default="+ Сравнить" data-added="В сравнении"></span></button>
											</div>
										  </div>
										  <div class="product-preview__bottom-pane">
											<a class="product-preview__name product-preview__for-tile-view" href="item.html" title="Atlas Vibration Acu-Node Massager"><span>Atlas Vibration Acu-Node Massager</span></a>
											<div class="product-preview__description product-preview__for-list-view">
											  <p>HoMedics® Портативный массаж спины подушки обеспечивает бодрящий массаж вибрации для всей спине. В комплекте с комплексной системой управления, позволяющей выбирать между высоким или низким настроек массажа, задняя массажные подушки идеально подходит для любого случая.</p>
											  <p><a class="button primary tiny" href="item.html" title="Atlas Vibration Acu-Node Massager"><span>ПОДРОБНЕЕ</span></a></p>
											</div>
											<a class="product-preview__rating rating product-preview__for-tile-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="4">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>24</ins> Отзыва</span>
											</a>
											<span class="product-preview__price price">
											  <span class="price__old"><ins>940</ins> грн</span>
											  <span class="price__current"><ins>800</ins> грн</span>
											</span>
											<button class="product-preview__button button button--buy button--gray expanded mfiB" type="button" data-param='{"action": "new", "id": 123}' title="Добавить товар в корзину"><span>КУПИТЬ</span> <i>
											  <svg>
												<use xlink:href="#icon_plus"></use>
											  </svg>
											</i></button>
											<a class="product-preview__button product-preview__for-tile-view button primary tiny" href="item.html" title="Atlas Vibration Acu-Node Massager"><span>ПОДРОБНЕЕ</span></a>
										  </div>
										</div>
									  </div>
									  <div class="carousel__item">
										<div class="carousel__product-preview product-preview">
										  <a class="product-preview__image-container" href="item.html" title="Body Massager with Perfect Reach Handle">
											<img src="pic/img-lot-3.jpg" alt="Body Massager with Perfect Reach Handle">
										  </a>
										  <div class="product-preview__top-pane">
											<a class="product-preview__name product-preview__name--large product-preview__for-list-view" href="item.html"
											   title="Body Massager with Perfect Reach Handle"><span>Body Massager with Perfect Reach Handle</span></a>
											<a class="product-preview__rating rating product-preview__for-list-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="5">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>200</ins> Отзыва</span>
											</a>
											<div class="product-preview__compare-wrap">
											  <span class="product-preview__code">UHE-XF43</span>
											  <button class="product-preview__compare switchable-button js-compare-add" data-product-id="003" type="button" title="Добавить товар к сравненю"><span class="switchable-button__text" data-default="+ Сравнить" data-added="В сравнении"></span></button>
											</div>
										  </div>
										  <div class="product-preview__bottom-pane">
											<a class="product-preview__name product-preview__for-tile-view" href="item.html" title="Body Massager with Perfect Reach Handle"><span>Body Massager with Perfect Reach Handle</span></a>
											<div class="product-preview__description product-preview__for-list-view">
											  <p>HoMedics® Портативный массаж спины подушки обеспечивает бодрящий массаж вибрации для всей спине. В комплекте с комплексной системой управления, позволяющей выбирать между высоким или низким настроек массажа, задняя массажные подушки идеально подходит для любого случая.</p>
											  <p><a class="button primary tiny" href="item.html" title="Body Massager with Perfect Reach Handle"><span>ПОДРОБНЕЕ</span></a></p>
											</div>
											<a class="product-preview__rating rating product-preview__for-tile-view" href="item.html#pageTab2" title="Перейти к отзывам">
											<span class="rating__stars" data-rating="5">
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											  <i>
												<svg>
												  <use xlink:href="#icon_star"></use>
												</svg>
											  </i>
											</span>
											  <span class="rating__count"><ins>200</ins> Отзыва</span>
											</a>
											<span class="product-preview__price price">
											  <span class="price__old"><ins>2300</ins> грн</span>
											  <span class="price__current"><ins>1999</ins> грн</span>
											</span>
											<button class="product-preview__button button button--buy button--gray expanded mfiB" type="button" data-param='{"action": "new", "id": 123}' title="Добавить товар в корзину"><span>КУПИТЬ</span> <i>
											  <svg>
												<use xlink:href="#icon_plus"></use>
											  </svg>
											</i></button>
											<a class="product-preview__button product-preview__for-tile-view button primary tiny" href="item.html" title="Body Massager with Perfect Reach Handle"><span>ПОДРОБНЕЕ</span></a>
										  </div>
										</div>
									  </div>
									</div>
									<div class="carousel__arrows">
									  <div class="carousel__arrows-track">
										<button class="carousel__arrow carousel__arrow--prev js-carousel-prev" type="button" title="Назад"><i></i><span
												class="show-for-sr">Назад</span></button>
										<button class="carousel__arrow carousel__arrow--next js-carousel-next" type="button" title="Вперёд">
										  <i></i><span class="show-for-sr">Вперёд</span></button>
									  </div>
									</div>
								  </div>
								</div>
								
							  </div>
							</div>
						  </div>
						</div> <!-- /page-section -->
						<div class="page-section">
						  <div class="row xlarge-collapse">
							<div class="column small-12">
							  <div id="seoClone" class="seo-clone"></div>
							</div>
						  </div>
						</div> <!-- /page-section -->
						
					</div>
					<footer class="page-footer">
					  <div class="page-footer__top-pane show-for-xlarge">
						<div class="row small-collapse">
						  <div class="column small-3">
							<a class="logo page-footer__logo" href="index.html" title="Homedics">
							  <img src="pic/logo.png" alt="Homedics" class="logo__image">
							  <div class="logo__text">
								<p>№1 Лидер массажного <br> оборудования и <br> товаров для красоты</p>
							  </div>
							</a>
							<div class="socials">
							  <a href="#" title="facebook" class="socials__link"><i class="socials__icon fill-hover-fb">
								<svg>
								  <use xlink:href="#icon_facebook"></use>
								</svg>
							  </i></a>
							  <a href="#" title="twitter" class="socials__link"><i class="socials__icon fill-hover-tt">
								<svg>
								  <use xlink:href="#icon_twitter"></use>
								</svg>
							  </i></a>
							  <a href="#" title="youtube" class="socials__link socials__link--youtube"><i class="socials__icon fill-hover-yt">
								<svg>
								  <use xlink:href="#icon_youtube"></use>
								</svg>
							  </i></a>
							  <br>
							  <a href="#" title="pinterest" class="socials__link"><i class="socials__icon fill-hover-pt">
								<svg>
								  <use xlink:href="#icon_pinterest"></use>
								</svg>
							  </i></a>
							  <a href="#" title="instagram" class="socials__link socials__link--instagram"><i class="socials__icon fill-hover-fb">
								<svg>
								  <use xlink:href="#icon_instagram"></use>
								</svg>
							  </i></a>
							  <a href="#" title="google+" class="socials__link socials__link--gp"><i class="socials__icon fill-hover-gp">
								<svg>
								  <use xlink:href="#icon_google_plus"></use>
								</svg>
							  </i></a>
							  <a href="#" title="blog" class="socials__link socials__link--blog">
								<span class="font-size-tiny font-family-georgia"><em>наш</em></span>
								<span class="font-size-small font-family-gothic">- <b>БЛОГ</b> -</span>
							  </a>
							</div>
						  </div>
						  <div class="column small-3">
							<div class="page-footer__menu-header">ПОДДЕРЖКА</div>
							<ul class="page-footer__menu">
							  <li><a href="#" title="Доставка и оплата">Доставка и оплата</a></li>
							  <li><a href="#" title="Доставка и оплата">Гарантия и Сертификаты</a></li>
							  <li><a href="#" title="Вопрос-Ответ">Вопрос-Ответ</a></li>
							  <li><a href="#" title="Для партнеров">Для партнеров</a></li>
							  <li><a href="#" title="Центральный офис в Киеве">Центральный офис в Киеве</a></li>
							  <li><a href="#" title="Сервисный центр">Сервисный центр</a></li>
							</ul>
						  </div>
						  <div class="column small-3">
							<div class="page-footer__menu-header">КОМПАНИЯ</div>
							<ul class="page-footer__menu">
							  <li><a href="#" title="О компании">О компании</a></li>
							  <li><a href="#" title="Статьи">Статьи</a></li>
							  <li><a href="#" title="Отзывы">Отзывы</a></li>
							  <li><a href="#" title="Контакты">Контакты</a></li>
							</ul>
						  </div>
						  <div class="column small-3">
							<div class="page-footer__menu-header">КОНТАКТЫ</div>
							<ul class="page-footer__menu color-primary">
							  <li><a class="link link--tel" href="tel:+380932638748" title="+38 093 26 38 748">+38 093 26 38 748</a>
							  </li>
							  <li><a class="link link--tel" href="tel:+380932638748" title="+38 093 26 38 748">+38 093 26 38 748</a>
							  </li>
							  <li><a class="link link--tel" href="tel:+380508232565" title="+38 050 82 32 565">+38 050 82 32 565</a>
							  </li>
							  <li><a class="link link--tel" href="tel:+380508232565" title="+38 050 82 32 565">+38 050 82 32 565</a>
							  </li>
							</ul>
							<p>&nbsp;</p>
							<p class="font-size-small" style="color: #75a1b9;">Центральный офис: <br> г. Киев, пр-т Московский, 8/16</p>
						  </div>
						</div>
					  </div>
					  <div class="page-footer__middle-pane">
						<div class="email-subscription">
						  <div class="row large-collapse">
							<div class="column small-12 xlarge-3 text-center xlarge-text-left">
							  <div class="email-subscription__title"><span>Подпишись на новости и акции</span></div>
							</div>
							<div class="column small-12 xlarge-9">
							  <div class="email-subscription__form row column wForm wFormDef wFormDef--dark text-center xlarge-text-left" data-form="true">
								  <div class="wFormInput">
									<input type="email" class="wInput" required data-rule-email="true" data-msg-email="Пожалуйста, введите корректный адрес электронной почты!" data-msg-required="Пожалуйста, заполните поле!" placeholder="your_email@example.com">
									<div class="inpInfo">email</div>
								  </div>
								  <button class="wSubmit button alert">ПОДПИСАТЬСЯ</button>
							  </div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="page-footer__bottom-pane">
						<div class="row large-collapse">
						  <div class="column small-12 large-6 text-center large-text-left">
							<p>© 2016. HoMedics Украина. Все права защищены.</p>
							<ul class="menu simple">
							  <li><a href="sitemap.html">Карта сайта</a></li>
							  <li><a href="#">Политика конфиденциальности</a></li>
							  <li><a href="#">Право собственности</a></li>
							</ul>
						  </div>
						  <div class="column small-12 large-6 text-center large-text-right">
							<a class="wezom" href="http://wezom.com.ua/" target="_blank" title="wezom.com.ua">Разработка сайта – студия Wezom <i><svg><use xlink:href="#icon_wezom"></use></svg></i></a>
						  </div>
						</div>
					  </div>
					</footer>
				</div>
			</div>
		</div>
	</div>
    <?php /*<div class="seoTxt" id="seoTxt">
        <div class="wSize wTxt">
            <?php echo $_content; ?>
        </div>
    </div>
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
        <div class="wConteiner">
            <div class="wSize">
                <?php echo Core\Widgets::get('Index_Slider'); ?>
                <?php echo Core\Widgets::get('Index_Banners'); ?>
                <?php echo Core\Widgets::get('Index_ItemsNew'); ?>
                <?php echo Core\Widgets::get('VK'); ?>
                <?php echo Core\Widgets::get('News'); ?>
                <?php echo Core\Widgets::get('Articles'); ?>
                <div class="clear"></div>
                <?php echo Core\Widgets::get('Index_ItemsPopular'); ?>
                <div id="clonSeo"></div>
            </div>
        </div>
    </div>*/?>
    <?php echo Core\Widgets::get('HiddenData'); ?>
    <?php //echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
</body>
</html>