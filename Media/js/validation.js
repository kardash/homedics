/*
    validation.js v2.0
    Wezom wTPL v3.0
*/

(function($){

    if (typeof wHTML === 'undefined') {
        window.wHTML = {};
    }

    wHTML.validation = function() {
        $('.wForm').each(function() {
            var formValid = $(this);
            if (formValid.data('disabled-submit')) {
                formValid.find('.wSubmit').prop('disabled',true);
                formValid.on('change', function () {
                    if (formValid.valid()) {
                        formValid.find('.wSubmit').prop('disabled',false);
                    } else {
                        formValid.find('.wSubmit').prop('disabled',true);
                    }
                })
            }
            formValid.validate({
                showErrors: function(errorMap, errorList) {
                    if (errorList.length) {
                        var s = errorList.shift();
                        var n = [];
                        n.push(s);
                        this.errorList = n;
                    }
                    this.defaultShowErrors();
                },
                invalidHandler: function(form, validator) {
                    formValid.addClass('no_valid');
                    $(formValid).data('validator').focusInvalid();
                },
                submitHandler: function(form) {
                    var $form = $(form);
                    formValid.removeClass('no_valid').addClass('success');
                    if (form.tagName.toLowerCase() === 'form') {
                        form.submit();
                    } else {
                        if( $form.data('ajax') ) {
                            if($form.data('loader')) {
                                loader($form.data('loader'), 1);
                            }
                            var data = new FormData();
                            var name;
                            var val;
                            var type;
                            $form.find('input,textarea,select').each(function(){
                                name = $(this).attr('data-name');
                                val = $(this).val();
                                type = $(this).attr('type');
                                if((type != 'checkbox' && name) || (type == 'checkbox' && $(this).prop('checked') && name)) {
                                    if(type == 'file') {
                                        data.append(name, $(this)[0].files[0]);
                                    } else if(type == 'radio' && $(this).prop('checked')) {
                                        data.append(name, val);
                                    } else if(type != 'radio') {
                                        data.append(name, val);
                                    }
                                }
                            });
                            var request = new XMLHttpRequest();
                            request.open("POST", '/form/' + $form.data('ajax'));
                            request.onreadystatechange = function() {
                                var status;
                                var resp;
                                if (request.readyState == 4) {
                                    status = request.status;
                                    resp = request.response;
                                    resp = jQuery.parseJSON(resp);
                                    if (status == 200) {
                                        if( resp.success ) {
                                            if (!resp.noclear) {
                                                $form.find('input').each(function(){
                                                    if( $(this).attr('type') != 'hidden' && $(this).attr('type') != 'checkbox' ) {
                                                        $(this).val('');
                                                    }
                                                });
                                                $form.find('textarea').val('');
                                            }
                                            if (resp.clear) {
                                                for(var i = 0; i < resp.clear.length; i++) {
                                                    $('input[name="' + resp.clear[i] + '"]').val('');
                                                    $('textarea[name="' + resp.clear[i] + '"]').val('');
                                                }
                                            }
                                            if (resp.insert && resp.insert.selector && resp.insert.html) {
                                                $(resp.insert.selector).html(resp.insert.html);
                                            }
                                            if ( resp.response ) {
                                                generate(resp.response, 'success', 3500);
                                            }
                                        } else {
                                            if ( resp.response ) {
                                                generate(resp.response, 'warning', 3500);
                                            }
                                        }
                                        if( resp.redirect ) {
                                            if(window.location.href == resp.redirect) {
                                                window.location.reload();
                                            } else {
                                                window.location.href = resp.redirect;
                                            }
                                        }
                                    } else {
                                        alert('Something went wrong,\nbut HTML fine ;)');
                                    }
                                }
                                if($form.data('loader')) {
                                    loader($form.data('loader'), 0);
                                }
                            };
                            request.send(data);
                            return false;
                        } else {
                            console.warn('HTML => Форма отправлена');
                        }
                    }
                }
            });
        });

        $('.wForm').on('change', '.wFile', function(event) {
            var m = $(this).prop('multiple'),
                f = this.files,
                label = $(this).siblings('.wFileVal'),
                t = label.data('txt');
            if (f.length && $(this).valid()) {
                if (m) {
                    var v = t[1].replace('%num%', f.length),
                        a = [];
                    for (var i = 0; i < f.length; i++) {
                        a.push(f[i].name);
                    }
                    label.html('<span>' + v + ' <ins>(' + a.join(', ') + ')</ins></span>');
                    $(this).blur();
                } else {
                    label.html(t[1] + ': ' + f[0].name);
                    $(this).blur();
                }
            } else {
                label.html(t[0]);
            }
        });


        /* Без тега FORM */
        $('.wForm').on('click', '.wSubmit', function(event) {
            var form = $(this).closest('.wForm');
            if (form.valid()) {
                form.submit();
            }
        });

        /* Сброс Без тега FORM */
        $('.wForm').on('click', '.wReset', function(event) {
            var form = $(this).closest('.wForm');
            if (form.is('DIV')) {
                form.validReset();
            }
        });
    };

})(jQuery);
