/*
 init.js v2.0
 Wezom wTPL v3.0
 */
window.wHTML = (function ($) {

    /* Приватные переменные */

    var varSeoIframe = 'seoIframe',
        varSeoTxt = 'seoTxt',
        varSeoClone = 'seoClone',
        varSeoDelay = 20;

    /* Приватные функции */

    /* проверка типа данных на объект */
    var _isObject = function (data) {
            var flag = (typeof data == 'object') && (data + '' != 'null');
            return flag;
        },

    /* создание нового элемента элемента */
        _crtEl = function (tag, classes, attrs, jq) {
            var tagName = tag || 'div';
            var element = document.createElement(tagName);
            var jQueryElement = jq || true;
            // если классы объявлены - добавляем
            if (classes) {
                var tagClasses = classes.split(' ');
                for (var i = 0; i < tagClasses.length; i++) {
                    element.classList.add(tagClasses[i]);
                }
            }
            // если атрибуты объявлены - добавляем
            if (_isObject(attrs)) {
                for (var key in attrs) {
                    var val = attrs[key];
                    element[key] = val;
                }
            }
            // возвращаем созданый елемент
            if (jQueryElement) {
                return $(element);
            } else {
                return element;
            }
        },

    /* создаем iframe для сео текста */
        _seoBuild = function (wrapper) {
            var seoTimer;
            // создаем iframe, который будет следить за resize'm окна
            var iframe = _crtEl('iframe', false, {
                id: varSeoIframe,
                name: varSeoIframe
            });
            iframe.css({
                'position': 'absolute',
                'left': '0',
                'top': '0',
                'width': '100%',
                'height': '100%',
                'z-index': '-1'
            });
            // добавляем его в родитель сео текста
            wrapper.prepend(iframe);
            // "прослушка" ресайза
            window.addEventListener('resize', function () {
                clearTimeout(seoTimer);
                seoTimer = setTimeout(function () {
                    wHTML.seoSet();
                }, varSeoDelay);
            }, false);
            // вызываем seoSet()
            wHTML.seoSet();
            setTimeout(function () {
                wHTML.seoSet();
            }, 500);
        };

    /* Публичные методы */

    function Methods() {
    }

    Methods.prototype = {

        /* установка cео текста на странице */
        seoSet: function () {
            if ($('#' + varSeoTxt).length) {
                var seoText = $('#' + varSeoTxt);
                var iframe = seoText.children('#' + varSeoIframe);
                if (iframe.length) {
                    // если iframe сущствует устанавливаем на место сео текст
                    var seoClone = $('#' + varSeoClone);
                    if (seoClone.length) {
                        // клонеру задаем высоту
                        seoClone.height(seoText.outerHeight(true));
                        // тексту задаем позицию
                        seoText.css({
                            top: seoClone.offset().top,
                            left: seoClone.offset().left,
                            width: seoClone.outerWidth(true)
                        });
                    } else {
                        // клонера нету - бьем в колокола !!!
                        console.error('"' + varSeoClone + '" - не найден!');
                    }
                } else {
                    // если iframe отсутствует, создаем его и устанавливаем на место сео текст
                    _seoBuild(seoText);
                }
            }
        },

        /* magnificPopup ajax */
        mfiAjax: function () {
            $('body').magnificPopup({
                delegate: '.mfiA',
                callbacks: {
                    elementParse: function (item) {
                        this.st.ajax.settings = {
                            url: item.el.data('url'),
                            type: 'POST',
                            data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                        };
                    },
                    ajaxContentAdded: function (el) {
                        wHTML.validation();
                        $('.inputmask').inputmask();
                    }
                },
                type: 'ajax',
                removalDelay: 300,
                mainClass: 'zoom-in'
            });
        },

        /* оборачивание iframe и video для адаптации */
        wTxtIFRAME: function () {
            var list = $('.wTxt').find('iframe').add($('.wTxt').find('video'));
            if (list.length) {
                // в цикле для каждого
                for (var i = 0; i < list.length; i++) {
                    var element = list[i];
                    var jqElement = $(element);
                    // если имеет класс ignoreHolder, пропускаем
                    if (jqElement.hasClass('ignoreHolder')) {
                        continue;
                    }
                    if (typeof jqElement.data('wraped') === 'undefined') {
                        // определяем соотношение сторон
                        var ratio = parseFloat((+element.offsetHeight / +element.offsetWidth * 100).toFixed(2));
                        if (isNaN(ratio)) {
                            // страховка 16:9
                            ratio = 56.25;
                        }
                        // назнчаем дату и обрачиваем блоком
                        jqElement.data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.toFixed(0) + '" style="padding-top:' + ratio + '%;""></div>');
                    }
                }
                // фиксим сео текст
                this.seoSet();
            }
        },

        /* Слайдер */
        setCarousel: function () {
            if ($('.js-carousel').length) {
                $('.js-carousel').each(function (index, el) {
                    var
                        carouselParent = $(el),
                        prev = carouselParent.find('.js-carousel-prev') || '',
                        next = carouselParent.find('.js-carousel-next') || '',
                        dots = carouselParent.find('.carousel__dots .row') || '',
                        autoplay = carouselParent.data('carousel-autoplay') || false,
                        autoplaySpeed = carouselParent.data('carousel-autoplay-speed') || 5000,
                        speed = carouselParent.data('carousel-speed') || 200,
                        breakpoint = carouselParent.data('breakpoint') || 'small',
                        carouselSettings,
                        carouselCurrentHeight;

                    var _carouselSet = function (options) {

                        var isActive = false;

                        var carousel = carouselParent.find('.js-carousel-track');

                        var _carouselInit = function () {
                            carousel
                                .on('init', function () {
                                    wHTML.seoSet();
                                })
                                .slick(options)
                                .on('beforeChange', function () {
                                    carouselCurrentHeight = carousel.height();
                                })
                                .on('afterChange', function () {
                                    if (carouselCurrentHeight !== carousel.height()) {
                                        wHTML.seoSet();
                                    }
                                });

                        };
                        if (Foundation.MediaQuery.atLeast(breakpoint)) {
                            _carouselInit(carousel);
                            isActive = true;
                        } else {
                            carouselParent.addClass('carousel-destroyed');
                        }

                        $(window).on('resize', function () {
                            if (!Foundation.MediaQuery.atLeast(breakpoint) && isActive) {
                                carousel.slick('unslick');
                                carouselParent.addClass('unslicked');
                                isActive = false;
                            } else if (Foundation.MediaQuery.atLeast(breakpoint) && !isActive) {
                                carouselParent.removeClass('carousel-destroyed');
                                _carouselInit(carousel);
                                isActive = true;
                            }
                        });
                    };

                    if (carouselParent.data('carousel-type') == 'show') {

                        carouselSettings = {
                            slidesToShow: 4,
                            arrows: true,
                            prevArrow: prev,
                            nextArrow: next,
                            pauseOnHover: true,
                            adaptiveHeight: true,
                            autoplay: autoplay,
                            autoplaySpeed: autoplaySpeed,
                            dots: false,
                            appendDots: dots,
                            infinite: true,
                            speed: speed,
                            responsive: [{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3
                                }
                            }, {
                                breakpoint: 720,
                                settings: {
                                    slidesToShow: 2
                                }
                            }, {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1
                                }
                            }]
                        };

                        _carouselSet(carouselSettings);

                    } else {
                        carouselSettings = {
                            arrows: true,
                            prevArrow: prev,
                            nextArrow: next,
                            pauseOnHover: true,
                            adaptiveHeight: true,
                            autoplay: autoplay,
                            autoplaySpeed: autoplaySpeed,
                            dots: false,
                            appendDots: dots,
                            infinite: true,
                            speed: speed
                        };

                        _carouselSet(carouselSettings);
                    }

                });
            }
        },
        /* radioToggle */
        radioToggle: function (item) {

            var $toggle = $(item);
            var targetArr = [];

            var _toggleTarget = function (toggle, target) {
                if (toggle.is(':checked') && (targetArr.indexOf(target) + 1)) {
                    targetArr.forEach(function (item, i, arr) {
                        if (item !== target) {
                            $(item).addClass('hide');
                        } else {
                            $(target).removeClass('hide');
                        }
                    })
                }
                wHTML.seoSet();
            };

            if ($toggle.length) {
                $toggle.each(function (index, el) {
                    targetArr.push($(el).data('target'));
                });
                _toggleTarget($toggle, $toggle.data('target'));
                $toggle.on('change', function () {
                    _toggleTarget($(this), $(this).data('target'));
                })
            }
        },
        /* checkToggle */
        checkToggle: function (item) {

            var $toggle = $(item);

            var _toggleTarget = function (toggle, target) {
                if (toggle.is(':checked')) {
                    $(target).removeClass('hide');
                } else {
                    $(target).addClass('hide');
                }
                wHTML.seoSet();
            };

            if ($toggle.length) {
                _toggleTarget($toggle, $toggle.data('target'));
                $toggle.on('change', function () {
                    _toggleTarget($(this), $(this).data('target'));
                })
            }
        }
    };

    /* Объявление wHTML и базовые свойства */

    var wHTML = $.extend(true, Methods.prototype, {});

    return wHTML;

})(jQuery);


jQuery(document).ready(function ($) {

        // поддержка cssanimations
        transitFlag = Modernizr.cssanimations;

        // очитска localStorage
        localStorage.clear();

        // сео текст
        wHTML.seoSet();

        // magnificPopup ajax
        wHTML.mfiAjax();

        // валидация форм
        wHTML.validation();

        // foundation
        $(document).foundation();

        $(document)
            .on('opened.zf.offcanvas', function () {
                $(this).find('html').addClass('is-zf-off-canvas-open').find('.off-canvas-wrapper').scrollTop('0');
            })
            .on('closed.zf.offcanvas', function () {
                $(this).find('html').removeClass('is-zf-off-canvas-open');
            })
            .on('down.zf.accordionMenu up.zf.accordionMenu change.zf.tabs down.zf.accordion up.zf.accordion', function () {
                wHTML.seoSet();
                $(document.body).trigger("sticky_kit:recalc");
            })
            .on('change.zf.tabs', function () {
                setTimeout(function () {
                    ratingProgress();
                }, 100)
            });

        // Animation
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

        // radioToggle
        wHTML.radioToggle('.js-radio-switch');

        // checkToggle
        wHTML.checkToggle('.js-check-switch');

        // Подменю
        if ($('.js-submenu').length) {
            var
                imgSrc,
                href,
                title;

            $('.js-submenu').on('mouseover', '.js-submenu-link', function () {
                var
                    $this = $(this),
                    $submenuContainer = $this.closest('.js-submenu'),
                    $submenuImg = $submenuContainer.find('.js-submenu-img'),
                    $submenuImgLink = $submenuContainer.find('.js-submenu-img-link'),
                    $submenuMainLink = $submenuContainer.find('.js-submenu-main-link');

                imgSrc = $this.data('img-src');
                href = $this.attr('href');
                title = $this.attr('title');

                if ($submenuImg.attr('src') != imgSrc) {
                    $submenuImg.attr('src', imgSrc).css('opacity', 0).stop().animate({
                        opacity: 1
                    }, 200);
                } else {
                    $submenuImg.attr('src', imgSrc);
                }

                if ($submenuImgLink.attr('href') != href) {
                    $submenuImgLink.attr({
                        'href': href,
                        'title': title
                    });
                }

                if ($submenuMainLink.attr('href') != href) {
                    $submenuMainLink.attr({
                        'href': href,
                        'title': title
                    });
                }

            });
        }

        // Переключение вида списка товаров
        if ($('#productCatalog').length) {
            var $catalog = $('#productCatalog'),
                $listBtn = $('#listBtn'),
                $tilesBtn = $('#tilesBtn');

            $listBtn.on('click', function () {
                $catalog.addClass('is-list-view');
                $(this).addClass('is-active');
                $tilesBtn.removeClass('is-active');
                wHTML.seoSet();
                $(document.body).trigger("sticky_kit:recalc");
            });
            $tilesBtn.on('click', function () {
                $catalog.removeClass('is-list-view');
                $(this).addClass('is-active');
                $listBtn.removeClass('is-active');
                wHTML.seoSet();
                $(document.body).trigger("sticky_kit:recalc");
            });
        }

        // Кнопка добавления отзыва
        $('.js-add-comment-button').on('click', function () {
            $(this).hide();
            $('#commentForm').slideDown();
        });

        // Sticky
        if ($('.js-sticky').length && Foundation.MediaQuery.atLeast('xlarge')) {
            $('.js-sticky').stick_in_parent({
                parent: '.js-sticky-parent',
                offset_top: 30
            });
        }

        // 360deg
        if ($('.js-3d-view-init').length) {
            var $threeSixtyInit = $('.js-3d-view-init'),
                dataFirstImage = $threeSixtyInit.data('first-image'),
                dataImages = $threeSixtyInit.data('images'),
                dataFrames = $threeSixtyInit.data('frames');

            $('.js-3d-view-init').magnificPopup({
                items: {
                    src: '<div class="mfiModal big framed"><img class="reel3d reel" src="' + dataFirstImage + '" width="auto" height="auto"/></div>',
                    type: 'inline'
                },
                removalDelay: 300,
                mainClass: 'zoom-in',
                callbacks: {
                    open: function () {
                        setTimeout(function () {
                            $('.reel3d').reel({
                                suffix: '',
                                images: dataImages,
                                frames: dataFrames,
                                responsive: true,
                                preloader: 5,
                                speed: 0.4,
                                cw: true
                            });
                        }, 1000)

                    },
                    close: function () {
                        $('.js-3d-image').unreel();
                    }
                }
            });
        }

        // Отображение полоски рейтинга
        ratingProgress = function () {
            if ($('.js-reviews-tab').hasClass('is-active')) {
                if ($('.js-rating-progress').length) {
                    var
                        rating = $('.js-rating-progress').data('comment-sum'),
                        singleRating;

                    $('.js-progress-bar').each(function () {
                        singleRating = $(this).data('comment-sum');
                        $(this).find('.js-progress-bar-meter').css('width', ((singleRating / rating) * 100).toFixed(2) + '%');
                    });
                }
            }
        };
        ratingProgress();

        // Галлерея товара
        if ($('#productImageContainer').length) {
            var
                $productImageContainer = $('#productImageContainer'),
                $productMainImage = $productImageContainer.find('img'),
                imgWidth = $productMainImage[0].naturalWidth,
                imgHeight = $productMainImage[0].naturalHeight;

            if ((imgWidth > $productImageContainer.innerWidth()) || (imgHeight > $productImageContainer.innerHeight())) {
                $productImageContainer.zoom();
            }
        }
        $('.js-product')
            .on('click', '.js-product-thumbnail', function (event) {
                event.preventDefault();
                var src = $(this).data('src');

                $productMainImage
                    .fadeOut(200, function () {
                        $productImageContainer.trigger('zoom.destroy');

                        $(this)
                            .attr('src', src)
                            .fadeIn(200);

                        imgWidth = this.naturalWidth;
                        imgHeight = this.naturalHeight;

                        if ((imgWidth > $productImageContainer.innerWidth()) || (imgHeight > $productImageContainer.innerHeight())) {
                            $productImageContainer.zoom({
                                url: src
                            });
                        }
                    });
            }).magnificPopup({
            delegate: '.js-product-video',
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });

        // Загрузка инструкций
        $('.js-instructions-select').on('change', function () {
            var $select = $('.js-instructions-select'),
                $curOption = $select.find('option:selected'),
                headerTxt = $select.data('header-txt'),
                footerTxt = $select.data('footer-txt'),
                infoTxt = $select.data('info-txt'),
                linkTitle = $select.data('link-title'),
                link = $curOption.data('url'),
                productTitle = $curOption.data('title');

            $.magnificPopup.open({
                items: {
                    src: '<div class="mfiModal medium instructions-popup">' +
                    '<div class="instructions-popup__header">' +
                    headerTxt + '<b class="instructions-popup__title">' + productTitle + '</b>' +
                    '</div>' +
                    '<div class="instructions-popup__body">' +
                    '<div class="row collapse">' +
                    '<div class="column">' +
                    '<div class="instructions-popup__button-container"><a class="button alert xlarge" href="' + link + '" title="' + linkTitle + ' ' + productTitle + '" target="_blank">СКАЧАТЬ</a></div>' +
                    '<div class="instructions-popup__info-container"><p>' + infoTxt + '</p></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="instructions-popup__footer">' +
                    '<div class="row"' +
                    '<div class="column"><p>' + footerTxt + '</p></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>',
                    type: 'inline'
                },
                removalDelay: 300,
                mainClass: 'zoom-in'
            });
        });

        // Добавление товара к сравнению и в избранное
        function compare(target, counter, url) {
            var
                $target = $(target),
                $counter = $(counter);
            $target.on('click', function (event) {
                event.preventDefault();
                var $that = $(this);
                $.ajax({
                    url: url,
                    method: 'post',
                    dataType: 'JSON',
                    data: {
                        productID: $(this).data('product-id'),
                        isAdded:  $(this).data('is-added')
                    },
                    success: function (response) {
                        if (response.success) {
                            // response.data = общее количество товаров добавленых к сравнению или в избранное
                            // response.isAdded = статус добавления (true,false)
                            // response.title = title кнопки после добавления товара
                            // response.link = ссылка на страницу сравнения или избранного
                            $counter
                                .html(response.data)
                                .addClass('animate')
                                .one(animationEnd, function () {
                                    $(this).removeClass('animate');
                                });
                            if(response.isAdded) {
                                $target.each(function (index, el) {
                                    if ($(el).data('product-id') === $that.data('product-id')) {
                                        $(el).addClass('is-added').attr('title',response.title).data('is-added',response.isAdded);
                                        $(el).on('click', function (e) {
                                            e.preventDefault();
                                            window.location = response.link
                                        })
                                    }
                                })
                            }
                        }
                    },
                    error: function (response, status) {
                        console.error('Ахтунг! Алярм! Капут!');
                    }
                });
            });
        }

        compare('.js-compare-add', '.js-compare-count', 'hidden/CompareAjax.php');
        compare('.js-favorite-add', '.js-favorite-count', 'hidden/FavoriteAjax.php');

        // magnify

        if ($('.js-gallery').length) {
            var $imgContainer;
            $('.js-gallery').each(function (index, el) {
                    $(el).magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        closeOnContentClick: false,
                        closeBtnInside: false,
                        mainClass: 'mfp-img-mobile magnify-gallery',
                        image: {
                            verticalFit: true,
                            titleSrc: function (item) {
                                return '<div class="font-family-gothic font-size-tiny">' + item.el.attr('title') + '&nbsp;&middot;&nbsp;<a class="color-primary" href="' + item.el.attr("data-source") + '" target="_blank">' + item.el.attr("data-open-link-title") + '</a></div>';
                            }
                        },
                        gallery: {
                            enabled: true
                        },
                        callbacks: {
                            imageLoadComplete: function () {
                                $imgContainer = $(this.content).find('.mfp-img').wrap('<span class="magnify-gallery-img"></span>').parent();
                                $imgContainer.zoom({
                                    url: this.currItem.src,
                                    on: 'grab'
                                });
                            },
                            change: function () {
                                if ($imgContainer) {
                                    $imgContainer.trigger('zoom.destroy');
                                    $imgContainer = 0;
                                }
                            }
                        }
                    })
                }
            )
        }

        $(window).load(function () {
            // оборачивание iframe и video для адаптации
            wHTML.wTxtIFRAME();

            // Скрол для таблиц в wTxt
            if ($('.wTxt table').length) {
                $('.wTxt table').wrap('<div style="overflow: auto;"></div>');
            }

            // слайдер
            wHTML.setCarousel();

            // Переход к отзывам
            if (window.location.hash == '#pageTab2') {
                $('#pageTab2-label').trigger('click');
                $('html, body').animate({
                    scrollTop: $('#pageTab2').offset().top - 50
                }, 1000);
            }

            // inputmask
            $('.inputmask').inputmask();

            // gmaps
            if ($('.js-map').length) {
                $('.js-map').each(function (index, el) {
                    $(el).wMap({
                        zoom: 17,
                        marker: {
                            icon: 'pic/map-marker.png'
                        }
                    });
                })
            }


        });

    }
);
