<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj->status); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[name]',
                            'value' => $obj->name,
                            'class' => 'valid',
                        ), 'Название'); ?>
                    </div>
					<?php $option = '';?>
					<?php /*foreach(\Core\Config::get('menu.groups') AS $group_id => $group_name): ?>
						<?php if ($group_id == $obj->group) { $select = 'selected'}?>
						<?php $option .= '<option value="'.$group_id.'" '.$group_id == $obj->group ? "selected" : NULL.'>'.$group_name.'</option>';?>
					<?php endforeach; ?>
					<div class="form-group">
                        <?php echo \Forms\Builder::select($option, NULL, array(
							'name' => 'FORM[group]',
						), 'Группа'); 
						?>
                    </div>*/?>
					 <div class="form-group">
                        <label class="control-label" for="f_group">Группа</label>
                        <div class="">
                            <select class="form-control valid" name="FORM[group]" id="f_group">
                                <?php foreach(\Core\Config::get('menu.groups') AS $group_id => $group_name): ?>
                                    <option value="<?php echo $group_id; ?>" <?php echo $group_id == $obj->group ? 'selected' : NULL; ?>><?php echo $group_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'id' => 'f_link',
                            'name' => 'FORM[url]',
                            'value' => $obj->url,
                            'class' => 'valid',
                        ), 'Ссылка'); ?>
                        <div class="thisLink"><span class="mainLink"><?php echo 'http://'.Core\Arr::get($_SERVER, 'HTTP_HOST'); ?></span><span class="samaLink"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>

<script type="text/javascript">
    function generate_link() {
        var link = $('#f_link').val();
        if(link != '') {
            if(link[0] != '/') {
                link = '/' + link;
            }
        }
        $('.samaLink').text(link);
    }
    $(document).ready(function(){
        generate_link();
        $('body').on('keyup', '#f_link', function(){ generate_link(); });
        $('body').on('change', '#f_link', function(){ generate_link(); });
    });
</script>